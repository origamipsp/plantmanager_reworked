const { Router } = require('express')
const settings = require('../controllers/settings.controller')
const delivery = require('../controllers/delivery.controller')
const goods = require('../controllers/goods.controller')
const goodsCategories = require('../controllers/goodsCategories.controller')
const supplier = require('../controllers/supplier.controller')
const customer = require('../controllers/customer.controller')

const supplierOrderImport = require('../controllers/supplierOrderImport.controller')
const goodsInSupplierOrder = require('../controllers/goodsInSupplierOrder.controller')
// const goodsInSupplierOrderIncome = require('../controllers/goodsInSupplierOrderIncome.controller')
const goodsIncome = require('../controllers/goodsIncome.controller')
const router = Router()

// '/api/'

router.post('/addUser', settings.addUser)
router.get('/getAllUsers', settings.getAllUsers)

router.post('/addDelivery', delivery.addDelivery)
router.get('/getAllDelivery', delivery.getAllDelivery)
router.post('/resaveDelivery', delivery.resaveDelivery)
router.post('/deleteDelivery', delivery.deleteDelivery)

router.post('/addGoods', goods.addGoods)
router.get('/getAllGoods', goods.getAllGoods)
router.post('/resaveGoods', goods.resaveGoods)
router.get('/getAllGoodsNameAndType', goods.getAllGoodsNameAndType)//
router.post('/deleteGoods', goods.deleteGoods)

router.post('/addGoodsCategory', goodsCategories.addGoodsCategory)
router.get('/getAllGoodsCategory', goodsCategories.getAllGoodsCategory)
router.post('/resaveGoodsCategory', goodsCategories.resaveGoodsCategory)

router.post('/addSupplier', supplier.addSupplier)
router.get('/getAllSuppliers', supplier.getAllSuppliers)
router.post('/resaveSupplier', supplier.resaveSupplier)
router.post('/deleteSupplier', supplier.deleteSupplier)
router.get('/getAllSuppliersForSelect', supplier.getAllSuppliersForSelect)

router.post('/addCustomer', customer.addCustomer)
router.get('/getAllCustomers', customer.getAllCustomers)
router.post('/resaveCustomer', customer.resaveCustomer)
router.post('/deleteCustomer', customer.deleteCustomer)

router.post('/addImportedOrders', supplierOrderImport.addImportedOrders)
router.get('/getAllImportedOrders', supplierOrderImport.getAllImportedOrders)
router.post('/resaveImportedOrders', supplierOrderImport.resaveImportedOrders)
// 
router.post('/addPositionInOrder', goodsInSupplierOrder.addGoodsInSupplierOrder)
router.post('/getAllGoodsInSupplierOrder', goodsInSupplierOrder.getAllGoodsInSupplierOrder)
router.post('/resaveGoodsInSupplierOrder', goodsInSupplierOrder.resaveGoodsInSupplierOrder)


// router.post('/addGoodsInSupplierOrderIncome', goodsInSupplierOrderIncome.addGoodsInSupplierOrderIncome)
// router.get('/getAllGoodsInSupplierOrderIncome', goodsInSupplierOrderIncome.getAllGoodsInSupplierOrderIncome)
// router.post('/resaveGoodsInSupplierOrderIncome', goodsInSupplierOrderIncome.resaveGoodsInSupplierOrderIncome)


router.post('/addGoodsIncome', goodsIncome.addGoodsIncome)
router.post('/getAllGoodsIncome', goodsIncome.getAllGoodsIncome)
router.post('/resaveGoodsIncome', goodsIncome.resaveGoodsIncome)

// router.post('/addGoodsIncome', goodsIncome.addGoodsIncome)
// router.post('/getAllGoodsIncome', goodsIncome.getAllGoodsIncome)
// router.post('/resaveGoodsIncome', goodsIncome.resaveGoodsIncome)



module.exports = router

module.exports = (sequelize, Sequelize) => {
    const Supplier = sequelize.define("table_supplier", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        company: {
            type: Sequelize.STRING,
        },
        phone: {
            type: Sequelize.STRING,
        },
        email: {
            type: Sequelize.STRING,
        },
        comment: {
            type: Sequelize.STRING,
        },
        address: {
            type: Sequelize.STRING,
        }, 
    },
    )
    return Supplier
}
module.exports = (sequelize, Sequelize) => {
    const GoodsInSupplierOrderImport = sequelize.define("table_goods_in_supplier_order_import", {


        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        idSupplierOrder: {
            type: Sequelize.INTEGER,
        },
        name: {
            type: Sequelize.STRING,
        },
        type: {
            type: Sequelize.STRING,
        },
        supplyStandart: {
            type: Sequelize.STRING,
        },
        //
        unit: {
            type: Sequelize.STRING,
        },
        countInPack: {
            type: Sequelize.STRING,
        },
        purchasePrice: {
            type: Sequelize.STRING,
        },
        ordered: {
            type: Sequelize.STRING,
        },
        sellPrice: {
            type: Sequelize.STRING,
        },
        status: {
            type: Sequelize.STRING,
        },
        deliveryTime: {
            type: Sequelize.STRING,
        },
        sum: {
            type: Sequelize.STRING,
        },
        markup: {
            type: Sequelize.STRING,
        },
    },
    )
    return GoodsInSupplierOrderImport
}
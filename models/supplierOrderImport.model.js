module.exports = (sequelize, Sequelize) => {
    const SupplierOrderImport = sequelize.define("table_supplier_order_import", {
        
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        date: {
            type: Sequelize.STRING,
        },
        title: {
            type: Sequelize.STRING,
        },
        supplier: {
            type: Sequelize.STRING,
        },
        //
        season: {
            type: Sequelize.STRING,
        },
        sum: {
            type: Sequelize.INTEGER,
        },
        payment: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.STRING,
        },
        deliveryPrice: {
            type: Sequelize.INTEGER,
        },
        packPrice: {
            type: Sequelize.INTEGER,
        },
        customsPrice: {
            type: Sequelize.INTEGER,
        },
    },
    )
    return SupplierOrderImport
}
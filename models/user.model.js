module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("table_users", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        remember_token: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        role: {
            type: Sequelize.INTEGER,
            defaultValue: null
        },

    },
        {
            indexes: [
                // Create a unique index
                {
                    unique: true,
                    fields: ['email']
                }
            ]
        }
    )

    return User
}
module.exports = (sequelize, Sequelize) => {
    const GoodsCategories = sequelize.define("table_goods_categories", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
        },
        parent_id: {
            type: Sequelize.INTEGER,
        },
    },
    )
    return GoodsCategories
}



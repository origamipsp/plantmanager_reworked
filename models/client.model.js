module.exports = (sequelize, Sequelize) => {
    const Client = sequelize.define("table_clients", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING
        },
        nickName: {
            type: Sequelize.STRING
        },
        tel: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        mail: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        source: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        comment: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        status: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate: Sequelize.NOW,
        },
        addr: {
            type: Sequelize.STRING,
            defaultValue: null
        },
    },
        // {
        //     indexes: [
        //         // Create a unique index
        //         {
        //             unique: true,
        //             fields: ['']
        //         }
        //     ]
        // }
    )

    return Client
}
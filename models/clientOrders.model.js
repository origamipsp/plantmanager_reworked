module.exports = (sequelize, Sequelize) => {
    const ClientOders = sequelize.define("table_clientOders", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        client_id: {
            type: Sequelize.INTEGER,
        },
        payType: {
            type: Sequelize.STRING
        },
        deliveryTime: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        status: {
            type: Sequelize.STRING,

        },
        total: {
            type: Sequelize.INTEGER,
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate: Sequelize.NOW,
        },
        comment: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        season: {
            type: Sequelize.STRING,
        },
        discount: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        },
        delivery_rate_id: {
            type: Sequelize.INTEGER,
        },
    },
    )

    return ClientOders
}
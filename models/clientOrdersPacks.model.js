module.exports = (sequelize, Sequelize) => {
    const ClientOdersPacks = sequelize.define("table_clientOdersPacks", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        clientOrderId: {
            type: Sequelize.INTEGER,
        },
        statusOfPack: {
            type: Sequelize.STRING
        },
        packId: {
            type: Sequelize.STRING,
        },
        createdAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            onUpdate: Sequelize.NOW,
        },
    },
    )
    return ClientOdersPacks
}
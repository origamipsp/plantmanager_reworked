module.exports = (sequelize, Sequelize) => {
    const Goods = sequelize.define("table_goods", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        category: {
            type: Sequelize.STRING,
        },
        name: {
            type: Sequelize.STRING,
        },
        type: {
            type: Sequelize.STRING,
        },
        unit: {
            type: Sequelize.STRING,
        },
        season: {
            type: Sequelize.STRING,
        },
        standart: {
            type: Sequelize.STRING,
        },
        lastsupplier: {
            type: Sequelize.STRING,
        },
        lastpraice: {
            type: Sequelize.STRING,
        },
        markup: {
            type: Sequelize.STRING,
        },
        manufacturer: {
            type: Sequelize.STRING,
        },
        barcode: {
            type: Sequelize.STRING,
        },
        //
        weight: {
            type: Sequelize.STRING,
        },
        length: {
            type: Sequelize.STRING,
        },
        width: {
            type: Sequelize.STRING,
        },
        height: {
            type: Sequelize.STRING,
        },
        description: {
            type: Sequelize.STRING,
        },
        storing: {
            type: Sequelize.STRING,
        },
    },
    )
    return Goods
}



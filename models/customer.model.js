module.exports = (sequelize, Sequelize) => {
    const Customer = sequelize.define("table_customers", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
        },
        nickName: {
            type: Sequelize.STRING,
        },
        phone: {
            type: Sequelize.STRING,
        },
        email: {
            type: Sequelize.STRING,
        },
        source: {
            type: Sequelize.STRING,
        }, 
        address: {
            type: Sequelize.STRING,
        }, 
        comment: {
            type: Sequelize.STRING,
        }, 
    },
    )
    return Customer
}
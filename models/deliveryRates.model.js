module.exports = (sequelize, Sequelize) => {
    const DeliveryRates = sequelize.define("table_delivery_rates", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        company: {
            type: Sequelize.STRING,
        },
        tariff: {
            type: Sequelize.STRING,
        },
        price: {
            type: Sequelize.INTEGER,
        },
        
    },
    )
    return DeliveryRates
}
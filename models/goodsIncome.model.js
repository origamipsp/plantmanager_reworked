module.exports = (sequelize, Sequelize) => {
    const GoodsIncome = sequelize.define("table_goods_income", {


        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        idCurrentPosition: {
            type: Sequelize.INTEGER,
        },
        consignmentNotedate: {
            type: Sequelize.STRING,
        },
        consignmentNoteNumber: {
            type: Sequelize.STRING,
        },
        supplyDate: {
            type: Sequelize.STRING,
        },
        delivered: {
            type: Sequelize.STRING,
        },
        sum: {
            type: Sequelize.STRING,
        },
    },
    )
    return GoodsIncome
}
const config = require('config')
const Sequelize = require('sequelize')
const dbConfig = config.get('DBConn')

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    // operatorsAliases: false,
    define: {
        charset: 'utf8',
        collate: 'utf8_general_ci',
        timestamps: true
    },
    logging: dbConfig.logging,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    },
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize





///



// db.company = require('./client.model')(sequelize, Sequelize)
// db.vacancy = require('./clientOrders.model')(sequelize, Sequelize)
// db.scammer = require('./clientOrdersPacks.model')(sequelize, Sequelize)
db.delivery = require('./deliveryRates.model')(sequelize, Sequelize)
db.user = require('./user.model')(sequelize, Sequelize)
db.goods = require('./goods.model')(sequelize, Sequelize)
db.goodsCategory = require('./goodsCategory.model')(sequelize, Sequelize)
db.supplier = require('./supplier.model')(sequelize, Sequelize)
db.customer = require('./customer.model')(sequelize, Sequelize)

db.supplierOrderImport = require('./supplierOrderImport.model')(sequelize, Sequelize)

db.goodsInSupplierOrderImport= require('./goodsInSupplierOrderImport.model')(sequelize, Sequelize)
// db.goodsInSupplierOrderIncome= require('./goodsInSupplierOrderIncome.model')(sequelize, Sequelize)
db.goodsIncome= require('./goodsIncome.model')(sequelize, Sequelize)

//  db.supplierOrderImport.hasOne(db.goodsInSupplierOrderImport, { through: db.role_user, onDelete: 'CASCADE' })
// db.role.belongsToMany(db.user, { through: db.role_user, onDelete: 'CASCADE' })


module.exports = db
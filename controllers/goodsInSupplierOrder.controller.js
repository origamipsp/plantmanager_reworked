const db = require('../models/index.js')
const GoodsInSupplierOrder = db.goodsInSupplierOrderImport


async function addGoodsInSupplierOrder(req, res) {
   console.log("addGoodsInSupplierOrder", req.body)
  try {
    const goodsInSupplierOrder = await GoodsInSupplierOrder.create(
      {
        idSupplierOrder: req.body.props.id,
        name: req.body.name,
        type: req.body.type,
        supplyStandart: req.body.supplyStandart,
        unit: req.body.unit,
        countInPack: req.body.countInPack,
        purchasePrice: req.body.purchasePrice,
        ordered: req.body.ordered,
        sellPrice: req.body.sellPrice,
        status: req.body.status,
        deliveryTime: req.body.deliveryTime,
        sum: req.body.sum,
        markup: req.body.markup,
    }
    );
    const result = await GoodsInSupplierOrder.findAll( {
      where: {
      idSupplierOrder: req.body.props.id
      }
    })
    res.status(200).json(result)
  } catch (error) {
    console.log("addGoodsInSupplierOrder-",error)
  }
}



async function getAllGoodsInSupplierOrder(req, res) {
  console.log("req-", req.body)
  try {
    const allGoods = await GoodsInSupplierOrder.findAll({
      where: {
        idSupplierOrder: req.body.id
      }
    })
    res.status(200).json({ allGoods })
  } catch (error) {
    console.log(error)
  }
}






async function resaveGoodsInSupplierOrder(req, res) {
  console.log("resaveGoodsInSupplierOrder-", req.body)
  try {
    const {
      id,
      idSupplierOrder,
      name,
      type,
      supplyStandart,
      unit,
      countInPack,
      purchasePrice,
      ordered,
      sellPrice,
      status,
      deliveryTime,
      sum,
      markup,

    } = req.body
    const goodsInSupplierOrderImportUpd = await GoodsInSupplierOrder.update(
      {
        idSupplierOrder,
        name,
        type,
        supplyStandart,
        unit,
        countInPack,
        purchasePrice,
        ordered,
        sellPrice,
        status,
        deliveryTime,
        sum,
        markup,
      },
      {
        where: { id },
      }
    );
    res.status(200).json({ goodsInSupplierOrderImportUpd })
  } catch (error) {
    console.log(error)
  }
}

module.exports = {
  addGoodsInSupplierOrder, getAllGoodsInSupplierOrder, resaveGoodsInSupplierOrder
}
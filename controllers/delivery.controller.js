const db = require('../models/index.js')
const DeliveryRates = db.delivery


async function addDelivery(req, res) {
  // console.log("addDelivery-", req.body)
  try {
    const deliveryRates = await DeliveryRates.create(
      {
        company: req.body.company,
        tariff: req.body.tariff,
        price: req.body.price
      }
    );
    const result = await DeliveryRates.findAll()
    res.status(200).json(result)
  } catch (error) {
    console.log(error)
  }

}



async function getAllDelivery(req, res) {
  // console.log("getAllDelivery-", req.body)
  try {
    const deliveries = await DeliveryRates.findAll({})
    // console.log(deliveries)
    res.status(200).json({ deliveries })
  } catch (error) {
    console.log(error)
  }
}




async function resaveDelivery(req, res) {
  // console.log("resaveDelivery-", req.body)
  try {
    const { id, company, tariff, price } = req.body
    const deliveryUpd = await DeliveryRates.update(
      {
        company,
        tariff,
        price,
      },
      {
        where: { id },
      }
    );
    res.status(200).json({ deliveryUpd })
  } catch (error) {
    console.log(error)
  }
}


async function deleteDelivery(req, res) {
  await DeliveryRates.destroy({
    where: { id: req.body.row.id },
  });
  const deliveries = await DeliveryRates.findAll({})
  res.status(200).json(deliveries)
}

module.exports = {
  addDelivery, getAllDelivery, resaveDelivery, deleteDelivery
}
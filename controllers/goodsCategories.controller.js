const db = require('../models/index.js')
const GoodsCategory = db.goodsCategory


async function addGoodsCategory(req, res) {
  // console.log("addGoodsCategory-", req.body)
  try {
    const [goodsCategory, created] = await GoodsCategory.findOrCreate({
      where: {
        name: req.body.name
      },
      defaults: {
        name: req.body.name,
        parentId: req.body.parentId,
      }
    });
    const result = await GoodsCategory.findAll()
    res.status(200).json(result)
  } catch (error) {
    comsole.log(error)
  }
}





async function getAllGoodsCategory(req, res) {
  // console.log("getAllGoodsCategory-", req.body)
  try {
    const goodsCategory = await GoodsCategory.findAll({})
    // console.log("getAllGoodsCategory-", goodsCategory)
    let goodsCategoryResultArr = []
    for (let i = 0; i < goodsCategory.length; i++) {
      goodsCategoryResultArr.push({ id: goodsCategory[i].dataValues.id, value: goodsCategory[i].dataValues.name, label: goodsCategory[i].dataValues.name, parent_id: goodsCategory[i].dataValues.parent_id })
    }
    // console.log("goodsCategoryResultArr--", goodsCategoryResultArr)
    res.status(200).json({ goodsCategoryResultArr })
  } catch (error) {
    console.log(error)
  }
}



async function resaveGoodsCategory(req, res) {
  // console.log("resaveGoods-", req.body)
  try {
    const {
      id,
      name,
      parentId,
    } = req.body
    const goodsCategoryUpd = await GoodsCategory.update(
      {
        name,
        parentId,
      },
      {
        where: { id },
      }
    );
    res.status(200).json({ goodsCategoryUpd })
  } catch (error) {
    console.log(error)
  }
}

module.exports = {
  addGoodsCategory, getAllGoodsCategory, resaveGoodsCategory
}
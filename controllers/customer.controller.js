const db = require('../models/index.js')
const Customer = db.customer


async function addCustomer(req, res) {
  // console.log("addCustomer-", req.body)
  try {
    const [customer, created] = await Customer.findOrCreate({
      where: {
        phone: req.body.phone
      },
      defaults: {
        name: req.body.name,
        nickName: req.body.nickName,
        phone: req.body.phone,
        email: req.body.email,
        source: req.body.source,
        address: req.body.address,
        comment: req.body.comment
      }
    });
    const result = await Customer.findAll()
    res.status(200).json(result)
  } catch (error) {
    console.log(error)
  }
}


async function getAllCustomers(req, res) {
  // console.log("getAllCustomers-", req.body)
  try {
    const customers = await Customer.findAll({})
    // console.log(customers)
    res.status(200).json({ customers })
  } catch (error) {
    console.log(error)
  }
}


async function resaveCustomer(req, res) {
  // console.log("resaveCustomer-", req.body)
  try {
    const { id, name, nickName, phone, email, source, address, comment } = req.body
    const customerUpd = await Customer.update(
      {
        name,
        nickName,
        phone,
        email,
        source,
        address,
        comment
      },
      {
        where: { id },
      }
    );
    res.status(200).json({ customerUpd })
  } catch (error) {
    console.log(error)
  }
}

async function deleteCustomer(req, res) {
  await Customer.destroy({
    where: { id: req.body.row.id },
  });
  const customers = await Customer.findAll({})
  res.status(200).json( customers )
}

module.exports = {
  addCustomer, getAllCustomers, resaveCustomer, deleteCustomer
}
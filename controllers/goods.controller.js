const db = require('../models/index.js')
const Goods = db.goods


async function addGoods(req, res) {
  // console.log("addGoods-", req.body)
  try {
    const [goods, created] = await Goods.findOrCreate({
      where: {
        name: req.body.name
      },
      defaults: {
        category: req.body.category,
        name: req.body.name,
        type: req.body.type,
        unit: req.body.unit,
        season: req.body.season,
        standart: req.body.standart,
        lastsupplier: req.body.lastsupplier,
        lastpraice: req.body.lastpraice,
        markup: req.body.markup,
        manufacturer: req.body.manufacturer,
        barcode: req.body.barcode,
        //
        weight: req.body.weight,
        length: req.body.length,
        width: req.body.width,
        height: req.body.height,
        description: req.body.description,
        storing: req.body.storing,
      }
    });
    const result = await Goods.findAll()
    res.status(200).json(result)
  } catch (error) {
    comsole.log(error)
  }

}





async function getAllGoods(req, res) {
  // console.log("getAllGoods-", req.body)
  try {
    const goods = await Goods.findAll({})
    // console.log("getAllGoods-", goods)
    res.status(200).json({ goods })
  } catch (error) {
    console.log(error)
  }
}

async function getAllGoodsNameAndType(req, res) {
   console.log("getAllGoodsCategory-")
  try {
    const goodsNameAndType = await Goods.findAll({})
    // console.log("getAllGoodsCategory-", goodsCategory)
    let goodsNameAndTypeResultArr = []
    for (let i = 0; i < goodsNameAndType.length; i++) {
      goodsNameAndType.push({ value: goodsNameAndType[i].dataValues.name, label: goodsNameAndType[i].dataValues.type})
    }
     console.log("goodsNameAndTypeResultArr--", goodsNameAndTypeResultArr)
    res.status(200).json({ goodsNameAndTypeResultArr })
  } catch (error) {
    console.log(error)
  }
}





async function resaveGoods(req, res) {
  // console.log("resaveGoods-", req.body)
  try {

    const {
      id,
      category,
      name,
      type,
      unit,
      season,
      standart,
      lastsupplier,
      lastpraice,
      markup,
      manufacturer,
      barcode,
      // 
      weight,
      length,
      width,
      height,
      description,
      storing,

    } = req.body

    const goodsUpd = await Goods.update(
      {
        category,
        name,
        type,
        unit,
        season,
        standart,
        lastsupplier,
        lastpraice,
        markup,
        manufacturer,
        barcode,
        // 
        weight,
        length,
        width,
        height,
        description,
        storing,
      },
      {
        where: { id },
      }
    );
    res.status(200).json({ goodsUpd })
  } catch (error) {
    console.log(error)
  }
}


async function deleteGoods(req, res) {
  await Goods.destroy({
    where: { id: req.body.row.id },
  });
  const goods = await Goods.findAll({})
  res.status(200).json( goods )
}

module.exports = {
  addGoods, getAllGoods, resaveGoods, deleteGoods, getAllGoodsNameAndType
}
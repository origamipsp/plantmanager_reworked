const db = require('../models/index.js')
const User = db.user


async function addUser(req, res) {
  const [user, created] = await User.findOrCreate({
    where: {
      email: req.body.email
    },
    defaults: { name: req.body.name, email: req.body.email,  password: req.body.password, remember_token: "", role: 0}
  });
}


async function getAllUsers(req, res) {
  try {
    const users = await User.findAll({})
    
    // console.log(users)
   
    res.status(200).json({ users })
  } catch (error) {
    console.log(error)
  }
}


module.exports = {
  addUser, getAllUsers
}
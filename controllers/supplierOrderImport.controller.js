const db = require('../models/index.js')
const SupplierOrderImport = db.supplierOrderImport


async function addImportedOrders(req, res) {
  try {
    const supplierOrderImport = await SupplierOrderImport.create(
      {
        date: req.body.date,
        title: req.body.title,
        supplier: req.body.supplier,
        season: req.body.season,
        sum: req.body.sum,
        payment: req.body.payment,
        status: req.body.status,
        
        deliveryPrice: req.body.deliveryPrice,
        packPrice: req.body.packPrice,
        customsPrice: req.body.customsPrice, 
    }
    );
    const result = await SupplierOrderImport.findAll()
    res.status(200).json(result)
  } catch (error) {
    console.log(error)
  }

}



async function getAllImportedOrders(req, res) {
 
  try {
    const sup = await SupplierOrderImport.findAll({})
    res.status(200).json({ sup })
  } catch (error) {
    console.log(error)
  }
}




async function resaveImportedOrders(req, res) {
  try {
    const { 
      id, 
      date,
      title,
      supplier,
      season,
      sum,
      payment,
      status,
      
      deliveryPrice,
      packPrice,
      customsPrice,
    
    } = req.body
    const supplierOrderImportUpd = await SupplierOrderImport.update(
      {
        date,
        title,
        supplier,
        season,
        sum,
        payment,
        status,
        
        deliveryPrice,
        packPrice,
        customsPrice,
      },
      {
        where: { id },
      }
    );
    res.status(200).json({ supplierOrderImportUpd })
  } catch (error) {
    console.log(error)
  }
}

module.exports = {
  addImportedOrders, getAllImportedOrders, resaveImportedOrders
}
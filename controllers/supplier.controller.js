const db = require('../models/index.js')
const Supplier = db.supplier


async function addSupplier(req, res) {
  try {
    const supplier = await Supplier.create(
      {
        company: req.body.company,
        phone: req.body.phone,
        email: req.body.email,
        comment: req.body.comment,
        address: req.body.address
      }
    );
    const result = await Supplier.findAll()
    res.status(200).json(result)
  } catch (error) {
    console.log(error)
  }
}


async function getAllSuppliers(req, res) {
  try {
    const suppliers = await Supplier.findAll({})
    res.status(200).json({ suppliers })
  } catch (error) {
    console.log(error)
  }
}


async function getAllSuppliersForSelect(req, res) {
  try {
    const suppliersForSelect = await Supplier.findAll({})
    let result = {}
    for (let i = 0; i < suppliersForSelect.length; i++) {
      result[i + 1] = suppliersForSelect[i].dataValues.company
    }
    res.status(200).json({ result })
  } catch (error) {
    console.log(error)
  }
}



async function resaveSupplier(req, res) {
  try {
    const { id, company, phone, email, comment, address } = req.body
    const supplierUpd = await Supplier.update(
      {
        company,
        phone,
        email,
        comment,
        address,
      },
      {
        where: { id },
      }
    );
    res.status(200).json({ supplierUpd })
  } catch (error) {
    console.log(error)
  }
}



async function deleteSupplier(req, res) {
  await Supplier.destroy({
    where: { id: req.body.row.id },
  });
  const suppliers = await Supplier.findAll({})
  res.status(200).json(suppliers)
}



module.exports = {
  addSupplier, getAllSuppliers, resaveSupplier, deleteSupplier, getAllSuppliersForSelect
}
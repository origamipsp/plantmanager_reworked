const db = require('../models/index.js')
const GoodsIncome = db.goodsIncome


async function addGoodsIncome(req, res) {
  // console.log("addGoodsInSupplierOrderImport-", req.body)
  try {
    const [goodsIncome, created] = await GoodsIncome.findOrCreate({
      where: {
        name: req.body.name
      },
      default: {
        idSupplierOrder: req.body.idSupplierOrder,
        name: req.body.name,
        type: req.body.type,
        supplyStandart: req.body.supplyStandart,
        unit: req.body.unit,
        countInPack: req.body.countInPack,
        purchasePrice: req.body.purchasePrice,
        ordered: req.body.ordered,
        sellPrice: req.body.sellPrice,
        status: req.body.status,
        deliveryTime: req.body.deliveryTime,
        sum: req.body.sum,
        markup: req.body.markup,
      }
    });
    const result = await GoodsInSupplierOrderIncome.findAll()
    res.status(200).json(result)
  } catch (error) {
    console.log(error)
  }
}



async function getAllGoodsIncome(req, res) {
  console.log("req.body",req.body)
  try {
    const allGoodsIncome = await GoodsIncome.findAll({
      where: {
        idCurrentPosition: req.body.id
      }
    })
    res.status(200).json({ allGoodsIncome })
  } catch (error) {
    console.log(error)
  }
}




async function resaveGoodsIncome(req, res) {
  // console.log("GoodsInSupplierOrderIncome-", req.body)
  try {
    const {
      id,
      idSupplierOrder,
      name,
      type,
      supplyStandart,
      unit,
      countInPack,
      purchasePrice,
      ordered,
      sellPrice,
      status,
      deliveryTime,
      sum,
      markup,

    } = req.body
    const goodsIncomeUpd = await GoodsIncome.update(
      {
        idSupplierOrder,
        name,
        type,
        supplyStandart,
        unit,
        countInPack,
        purchasePrice,
        ordered,
        sellPrice,
        status,
        deliveryTime,
        sum,
        markup,

      },
      {
        where: { id },
      }
    );
    res.status(200).json({ goodsIncomeUpd })
  } catch (error) {
    console.log(error)
  }
}

module.exports = {
  addGoodsIncome, getAllGoodsIncome, resaveGoodsIncome
}
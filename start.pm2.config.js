// Запуск этого приложения: pm2 start site.config.js

module.exports = {
    apps: [{
        name: "plant",
        script: "./app.js",
        env: {
            NODE_ENV: "production",

            PORT: 5001
        },
        env_dev: {
            NODE_ENV: "production",

            PORT: 5001
        },
        out_file: "/dev/null",
        error_file: "/dev/null"
    }]
}
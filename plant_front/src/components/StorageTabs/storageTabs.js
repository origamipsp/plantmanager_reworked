
import { NavLink, useMatch, Link } from 'react-router-dom'
import './storageTabs.css';
import { Tabs, Tab } from "react-bootstrap";
import { useState } from 'react';
import TableVirtual from "../Tables/table_virtual_storage";
import TableReal from "../Tables/table_real_storage";
import TableStorageMove from "../Tables/table_storage_move";
import TableWriteOff from "../Tables/table_write_off";

export const StorageTabs = ({ className = '', visible = true }) => {
    const [key, setKey] = useState("virtual");
    const [activeRow, setActiveRow] = useState({});
    return (
        <div className='wrap'>
            <Tabs
                className="tabs"
                id="controlled-tab-example"
                activeKey={key}
                onSelect={(k) => setKey(k)}
            >
                <Tab eventKey="virtual" title="Виртуальный">
                    <p>Наименований: 3581 Приход: 37 317 шт. Расход: 20 006 шт. Остаток: 17 311 шт.</p>
                    <p>Приход закупочная сумма: 7 986 133,745 руб. Приход продажная сумма: 16 330 777 руб.</p>
                    <p>Расход закупочная сумма: 3 829 015,145 руб. Расход продажная сумма: 7 502 622 руб.</p>
                    <p>Остаток закупочная сумма: 4 157 118,6 руб. Остаток продажная сумма: 8 828 155 руб.</p>
                    <div>
                        <TableVirtual setActiveRow={setActiveRow} />
                    </div>
                </Tab>
                <Tab eventKey="real" title="Реальный">
                    <p>Наименований: 3203</p>
                    <p>Штук: 9649 шт.</p>
                    <p>На сумму: 2349896.90 руб.</p>
                    <div>
                        <TableReal setActiveRow={setActiveRow} />
                    </div>

                </Tab>

                <Tab eventKey="moving" title="Перемещение">
                <TableStorageMove setActiveRow={setActiveRow} />
                </Tab>
                <Tab eventKey="writeoff" title="Списание">
                <TableWriteOff setActiveRow={setActiveRow} />
                </Tab>
            </Tabs>
        </div>
    );
}
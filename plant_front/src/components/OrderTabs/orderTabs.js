import './orderTabs.css';
import { Tabs, Tab } from "react-bootstrap";
import { useState } from 'react';
import React from 'react'
import TableImport from "../Tables/table_import";
import TableIncome from "../Tables/table_income";



export const OrderTabs = ({ className = '', visible = true }) => {
  const [key, setKey] = useState("import");
  const [activeRow, setActiveRow] = useState({});

  return (
    <div className='wrap'>
      <Tabs
        className="tabs"
        id="controlled-tab-example"
        activeKey={key}
        onSelect={(k) => setKey(k)}
      >
        <Tab eventKey="import" title="Импорт">
          <div>
            <TableImport setActiveRow={setActiveRow} />
          </div>
        </Tab>

        <Tab eventKey="income" title="Приход">
          Упразднено
          {/* <TableIncome setActiveRow={setActiveRow} /> */}
        </Tab>

      </Tabs>
    </div>
  );
}
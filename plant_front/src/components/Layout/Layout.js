import { NavBar } from '../NavBar/NavBar'
import './layout.css';

const Layout = ({children, sidebar = true, footer = false, footerText = '', footerPerc = 0}) => {
  return (
    <div className="layout">
      <NavBar/>
      <div className="layout_wrapper">
       
        <main className="container-fluid h-100">
          {children}
        </main>
      </div>
      <footer className={`${!footer && 'd-none'}`}>
        <div className="absolute stepper" style={{width: `${footerPerc}vw`}}/>
        <span className="text-base">{footerText}</span>
      </footer>
    </div>
  )
}

export {Layout}
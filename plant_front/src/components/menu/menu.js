import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MenuIcon from "@material-ui/icons/Menu";

// import { useHistory } from "react-router-dom";

export default function SimpleMenu({ ...props }) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  //   let history = useHistory();
  // console.log("--", props.menuItems)


  const renderMenuItem = (menuItem, idx) => {
    // console.log(menuItem, idx)
    // console.log("inx", idx)

    return (

      <div>
        <MenuItem key={idx} onClick={menuItem.onClick[0]}>{menuItem.title[0]}</MenuItem>
        {/* <MenuItem key={idx} onClick={menuItem.onClick}>{menuItem.title[1]}</MenuItem> */}
      </div>

    )


  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (pathname) => {
    // history.push(pathname);
    setAnchorEl(null);
  };

  if (!props?.menuItems?.length) {
    return (<></>)
  }

  return (
    <div>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MenuIcon />
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {/* <MenuItem onClick={() => handleClose("")}>Отчёт</MenuItem>
        <MenuItem onClick={() => handleClose("")}>Удалить</MenuItem>
        <MenuItem onClick={() => handleClose("")}>Изменить</MenuItem> */}

        {props?.menuItems?.length ?
          props.menuItems.map(renderMenuItem)
          :
          <></>
        }


      </Menu>
    </div>
  );
}


import { NavLink, useMatch, Link } from 'react-router-dom'
import './navBar.css';

export const NavBar = ({ className = '', visible = true }) => {

  const selected = ({ isActive }) => {
    return isActive ? 'topNav_el topNav_active' : 'topNav_el'
  }

  return (
    <div className={`${className} topNav ${!visible && 'd-none'}`}>

      <div className="topNav">

        <table>
          <tbody>
            <tr>
              <td>USD: 56,5616 руб.</td>
              <td rowspan="2"><NavLink to="/" className="home"><h3>Plant Management</h3></NavLink></td>
            </tr>
            <tr>
              <td>EUR: 57,1526 руб</td>
            </tr>
          </tbody>
        </table>




      </div>

      <div className="menuWrap">
        <NavLink to="/orderstosuppliers" className={selected}><button className="btn btn-orange">Заказ поставщику</button></NavLink>
        <NavLink to="/storage" className={selected}><button className="btn btn-orange">Склад</button></NavLink>
        <NavLink to="/clientorder" className={selected}> <button className="btn btn-orange">Заказ Клиента</button></NavLink>
        <NavLink to="/customerdirectory" className={selected}><button className="btn btn-orange">Справочник "Клиенты"</button></NavLink>
        <NavLink to="/supplierdirectory" className={selected}><button className="btn btn-orange">Справочник "Поставщики"</button></NavLink>
        <NavLink to="/goodsdirectory" className={selected}><button className="btn btn-orange">Справочник "Товары"</button></NavLink>
        <NavLink to="/deliverydirectory" className={selected}><button className="btn btn-orange">Справочник "Доставка"</button></NavLink>
        <NavLink to="/settings/units" className={selected}><button className="btn btn-orange">Настройки</button></NavLink>
      </div>
    </div>
  )
}
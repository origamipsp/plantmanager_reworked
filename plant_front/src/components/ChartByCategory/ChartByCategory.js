
import { Pie } from "react-chartjs-2";
import './ChartByCategory.css';
export const ChartByCategory = () => {

    const options = {
        plugins: {
            title: {
                display: true,
                text: "Chart.js Bar Chart - Stacked"
            }
        }
        // responsive: true
    };


    const data = {
        labels: ["Red", "Blue", "Yellow"],
        datasets: [
            {
                label: "My First Dataset",
                data: [300, 50, 100],
                backgroundColor: [
                    "rgb(255, 99, 132)",
                    "rgb(54, 162, 235)",
                    "rgb(255, 205, 86)"
                ],
                hoverOffset: 4
            }
        ]
    };

    return (
        <div class="container">
            <h3 class="text-center">Заказы по категориям</h3>
            <div class="row">
                <div class="col">
                    <h5 class="text-center">Покупатели</h5>
                    <Pie data={data} options={options} />
                </div>
                <div class="col">
                    <h5 class="text-center">Поставщики</h5>
                    <Pie data={data} options={options} />
                </div>
            </div>
        </div>
    )
}





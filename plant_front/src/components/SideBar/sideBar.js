import './sideBar.css';
import { NavLink} from 'react-router-dom'
import { useHttp } from "../../hooks/http.hook"
import { Line } from '../Line/line'
import {
  Button,
  Modal,
} from "react-bootstrap";
import { useState } from 'react';

const SideBar = ({ className = '', visible = true }) => {

  const [show, setShow] = useState(false);
  const closeModal = () => setShow(false);
  const showModal = () => {
    setShow(true)
  }

  const selected = ({ isActive }) => {
    return isActive ? 'topNav_el topNav_active' : 'topNav_el'
  }

  const { request } = useHttp()

  const [name, setName] = useState(null)
  const [email, setEmail] = useState(null)
  const [password, setPassword] = useState(null)
  const [passwordRepeat, setPasswordRepeat] = useState(null)

  function addClick() {
    if(password == passwordRepeat){
      const data = request('/api/addUser', 'POST', { ...{ name: name, email: email, password: password }})
    } 
  }

  return (
    <>
      <div className={`${className} sideBar ${!visible && 'd-none'}`}>
        <NavLink to="/settings/units" className={selected}><button className="btn btn-orange side">Единицы измерения</button></NavLink>
        <NavLink to="/settings/deliverytime" className={selected}><button className="btn btn-orange side">Сроки поставки</button></NavLink>
        <NavLink to="/settings/seasonality" className={selected}> <button className="btn btn-orange side">Сезонность</button></NavLink>
        <NavLink to="/settings/customersource" className={selected}><button className="btn btn-orange side">Источники клиента</button></NavLink>
        <NavLink to="/settings/storageperiods" className={selected}><button className="btn btn-orange side">Сроки хранения</button></NavLink>
        <NavLink to="/settings/paymenttype" className={selected}><button className="btn btn-orange side">Тип оплаты</button></NavLink>
        <NavLink to="/settings/paymentpurpose" className={selected}><button className="btn btn-orange side">Назначение платежа</button></NavLink>
        <NavLink to="/settings/customerorderstatus" className={selected}><button className="btn btn-orange side">Статус заказа клиента</button></NavLink>
        <NavLink to="/settings/supplierorderstatus" className={selected}><button className="btn btn-orange side">Статус заказа поставщику</button></NavLink>
        <NavLink to="/settings/itemstatus" className={selected}><button className="btn btn-orange side">Статус товара</button></NavLink>
        <NavLink to="/settings/storages" className={selected}> <button className="btn btn-orange side">Склады</button></NavLink>
        <NavLink to="/settings/writeoffreasons" className={selected}><button className="btn btn-orange side">Причины списания</button></NavLink>
        <NavLink to="/settings/productcategories" className={selected}><button className="btn btn-orange side">Категории товара</button></NavLink>
        <Line />
        {/* <NavLink to="/settings/employeeregistration" className={selected}> */}
        <button className="btn btn-orange side" onClick={showModal}>Регистрация сотрудника</button>
        <NavLink to="/settings/users" className={selected}><button className="btn btn-orange side">Пользователи</button></NavLink>
      </div>



      <Modal size="m" scrollable
        aria-labelledby="contained-modal-title-vcenter"
        centered show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Регистрация сотрудника</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody">
          <form oninput='passwordRepeat.setCustomValidity(passwordRepeat.value != password.value ? "Passwords do not match." : "")'>
            <div class="form-group">
              <label for="name">Имя</label>
              <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Введите имя" onChange={(event) => setName(event.target.value)}></input>
            </div>
            <div class="form-group">
              <label for="email">Email адресс</label>
              <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Введите email" onChange={(event) => setEmail(event.target.value)}></input>
            </div>
            <div class="form-group">
              <label for="password">Пароль</label>
              <input type="password" class="form-control" id="password" name='password' aria-describedby="passwordHelp" placeholder="Введите пароль" onChange={(event) => setPassword(event.target.value)}></input>
            </div>
            <div class="form-group">
              <label for="password">Пароль</label>
              <input type="password" class="form-control" id="passwordRepeat" name="passwordRepeat" placeholder="Повторите пароль" onChange={(event) => setPasswordRepeat(event.target.value)}></input>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={addClick}>
            Добавить
          </Button>
          <Button variant="danger" onClick={closeModal} >
            Отмена
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
export default SideBar;
import React, { useEffect } from 'react';
import styled from 'styled-components';
import DataTable from "react-data-table-component";
import "./table.css";
import { dataGoods } from "./data_goods";
import Button from 'react-bootstrap/Button';
import SimpleMenu from '../menu/menu';
import { Line } from '../../components/Line/line'
import InputGroup from 'react-bootstrap/InputGroup';
import Dropdown from 'react-bootstrap/Dropdown'
import { data } from "./data_import";//
import { useState } from 'react';
import Select from 'react-select';
import { units } from '../../data/select_config_data';
import { deliveryTime } from '../../data/select_config_data';
import { useHttp } from "../../hooks/http.hook"
import { GoodsIncome } from "./table_income_goods";
import { itemStatus } from '../../data/select_config_data';
import {
  Form,
  FormGroup,
  Modal,
  Row
} from "react-bootstrap";



//<input className="form-control" type="date" value={row && row.date ? row.date : ""} id="example-date-input"></input>
const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <input
    // href=""
    ref={ref}
    value={""}
    className="form-control"
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
  </input>
));

//

const customStylesSelect = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,
  }),
  container: (provided, state) => ({
    ...provided,
    // width: 160,
    zIndex: 2,
  }),
}

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];


const CustomMenu = React.forwardRef(
  ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
    const [value, setValue] = useState('');
    return (
      <div
        ref={ref}
        style={style}
        className={className}
        aria-labelledby={labeledBy}
      >
        <Form.Control
          autoFocus
          className="mx-3 my-2 w-auto"
          placeholder="Type to filter..."
          onChange={(e) => setValue(e.target.value)}
          value={value}
        />
        <ul className="list-unstyled">
          {React.Children.toArray(children).filter(
            (child) =>
              !value || child.props.children.toLowerCase().startsWith(value),
          )}
        </ul>
      </div>
    );
  },
);


const TextField = styled.input`
  	height: 32px;
  	width: 200px;
  	border-radius: 3px;
  	border-top-left-radius: 5px;
  	border-bottom-left-radius: 5px;
  	border-top-right-radius: 0;
  	border-bottom-right-radius: 0;
  	border: 1px solid #e5e5e5;
  	padding: 0 16px 0 8px;
  
  	&:hover {
  		cursor: pointer;
  	}
  `;

const ClearButton = styled(Button)`
  	border-top-left-radius: 0;
  	border-bottom-left-radius: 0;
  	border-top-right-radius: 5px;
  	border-bottom-right-radius: 5px;
  	height: 32px;
  	width: 32px;
  	text-align: center;
  	display: flex;
  	align-items: center;
  	justify-content: center;
    // background-color: #FF0000;
    // border-color:#FF0000;
  `;


const FilterComponent = ({ filterText, onFilter, onClear }) => (
  <>
    <TextField
      id="search"
      type="text"
      placeholder="Фильтр по наименованию"
      aria-label="Search Input"
      value={filterText}
      onChange={onFilter}
    />
    <ClearButton type="button" onClick={onClear}>
      X
    </ClearButton>
  </>
);





const columnsGoods = [
  {
    name: "Наименование	",
    selector: "name",
    sortable: true,
    compact: true,
    // width: "105px",

  },
  {
    name: "Сорт",
    selector: "type",
    sortable: true,
    compact: true,
    // width: "65px",
  },
  {
    name: "Поставка",
    selector: "supplyStandart",
    sortable: true,
    compact: true,
    // width: "75px",
  },
  {
    name: "Ед. изм.",
    selector: "unit",
    sortable: true,
    compact: true,
    // width: "65px",
  },
  {
    name: "В уп.",
    selector: "countInPack",
    sortable: true,
    compact: true,
    // width: "55px",
  },

  {
    name: "Цена закуп.",
    selector: "purchasePrice",
    sortable: true,
    compact: true,
    // width: "100px",
  },
  {
    name: "Заказано",
    selector: "ordered",
    sortable: true,
    compact: true,
    // width: "75px",
  },
  {
    name: "Цена продаж.",
    selector: "sellPrice",
    sortable: true,
    compact: true,
    // width: "100px",
  },
  {
    name: "Статус",
    selector: "status",
    sortable: true,
    compact: true,
    // width: "70px",
  },
  {
    name: "Время поставки",
    selector: "deliveryTime",
    sortable: true,
    compact: true,
    // width: "110px",
  },

  {
    name: "Сумма	",
    selector: "sum",
    sortable: true,
    compact: true,
    // width: "65px",
  },
  {
    name: "Наценка, %",
    selector: "markup",
    sortable: true,
    compact: true,
    // width: "85px",
  },

  {
    name: "Действие",
    cell: row => <SimpleMenu size="small" row={row} menuItems={[{ title: ["Удалить"], onClick: () => { console.log("123qwe") } }]} />,
    allowOverflow: true,
    button: true,
    compact: true,
    // width: "55px",
  },
];

export const GoodsImport = (props) =>  {


  const ExpandedComponent = ({ data }) => 
  
 
  
  <pre>
    <h4>Изменение заказанного у поставщика</h4>
    <table>
      <tr>
        <td>Наименование</td>
        <td>Сорт</td>
        <td>Стандарт поставки</td>
        <td>Ед. изм.</td>
        <td>Кол-во в уп.</td>
        <td>Цена закупки</td>
        <td>Кол-во заказано</td>
        <td>Цена продажи</td>
        <td>Статус</td>
        <td>Время поставки</td>
        <td>Наценка, %</td>
        <td>Сумма</td>
      </tr>

      <tr>
        <td>
          <Select
            onChange={changeHandler}
            className="select"
            options={name}
            isSearchable="true"
            styles={customStylesSelect}
            name="name"
            value={formGoods.name}
          />
        </td>

        <td>
          <Select
            onChange={changeHandler}
            className="select"
            options={type}
            isSearchable="true"
            styles={customStylesSelect}
            name="name"
            value={formGoods.type}
          />
        </td>

  

        <td>
          <InputGroup >
            <Form.Control value={formGoods.supplyStandart}
              onChange={changeHandler}
               name="supplyStandart" />
          </InputGroup>
        </td>
        <td>

          <select className="form-select" aria-label="Default select example" name="unit"
            value={formGoods.unit}
            onChange={changeHandler}
          >
            <option value="" selected>
              {formGoods && formGoods.unit ? formGoods.unit : ""}
            </option>
            {Object.values(units).map((unit) => (
              <option value={unit}>{unit}</option>
            ))}
          </select>

          {/* <Select onChange={changeHandler} name="unit" className="select" options={options} isSearchable="true" placeholder={'Ед.изм.'} styles={customStylesSelect} /> */}
        </td>
        <td>
          <InputGroup >
            <Form.Control value={formGoods.countInPack}
              onChange={changeHandler} name="countInPack" />
          </InputGroup>
        </td>





        <td>
          <InputGroup >
            <Form.Control onChange={changeHandler}
              value={formGoods.purchasePrice} name="purchasePrice" />
          </InputGroup>
        </td>
        <td>
          <InputGroup >
            <Form.Control value={formGoods.ordered}
              onChange={changeHandler} name="ordered" />
          </InputGroup>
        </td>
        <td>
          <InputGroup >
            <Form.Control value={formGoods.sellPrice}
              onChange={changeHandler} name="sellPrice" />
          </InputGroup>
        </td>
        <td>
          <select className="form-select" aria-label="Default select example" name="status"
            onChange={changeHandler}
          >
            <option value="" selected>
              {formGoods && formGoods.status ? formGoods.status : ""}
            </option>
            {Object.values(itemStatus).map((status) => (
              <option value={status}>{status}</option>
            ))}
          </select>

          {/* <Select onChange={changeHandler} name="status" className="select" options={options} isSearchable="true" placeholder={'Статус'} styles={customStylesSelect} /> */}
        </td>
        <td>
          <select className="form-select"
            name="deliveryTime"
            onChange={changeHandler}
          >
            <option value="" selected>
              {formGoods && formGoods.deliveryTime ? formGoods.deliveryTime : ""}
            </option>
            {Object.values(deliveryTime).map((deliveryTime) => (
              <option value={deliveryTime}>{deliveryTime}</option>
            ))}
          </select>

          {/* <Select onChange={changeHandler} name="deliveryTime"  className="select" options={options} isSearchable="true" placeholder={'Время'} styles={customStylesSelect} /> */}
        </td>
        <td>
          <InputGroup >
            <Form.Control value={formGoods.sum}
              onChange={changeHandler} name="sum" className="select" />
          </InputGroup>
        </td>
        <td>
          <InputGroup >
            <Form.Control value={formGoods.markup}
              onChange={changeHandler} name="markup" className="select" />
          </InputGroup>
        </td>
      </tr>
    </table>

    <div className="d-flex justify-content-end">
      <button type="button" className="btn btn-warning btn-sm" onClick={clickEdit}>Изменить</button>


    </div>

    <GoodsIncome id={data.id} />
  </pre>;

  const { request } = useHttp()

  const [formGoods, setFormGoods] = useState({
    name: "",
    type: "",
    supplyStandart: "",
    unit: "",
    countInPack: "",
    purchasePrice: "",
    ordered: "",
    sellPrice: "",
    status: "",
    deliveryTime: "",
    sum: "",
    markup: "",
  })

  const [positions, setPositions] = useState(null)


  const [filterText, setFilterText] = React.useState('');
  const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
  const filteredItems = dataGoods.filter(
    item => item.name && item.name.toLowerCase().includes(filterText.toLowerCase()),
  );

  const [unit, setUnit] = useState("");
  const [delTime, setDelTime] = useState("");
  const [row, setRow] = useState(null);








  const [hide, setHide] = useState(true);
  const [goodsInSupplierOrderImport, setGoodsInSupplierOrderImport] = useState(null)



  const clickRow = (row, event) => {
    setCurrentPosition(row.id)
    setFormGoods(
      {
        name: row.name,
        type: row.type,
        supplyStandart: row.supplyStandart,
        unit: row.supplyStandart,
        countInPack: row.countInPack,
        purchasePrice: row.purchasePrice,
        ordered: row.ordered,
        sellPrice: row.sellPrice,
        status: row.status,
        deliveryTime: row.deliveryTime,
        sum: row.sum,
        markup: row.markup,
      })
    // setShowEdit(true)
    // setCurrentCompany(row.id)
  }


  const [currentPosition, setCurrentPosition] = useState(null)
    async function clickEdit() {
      console.log("form-", { ...formGoods })
      try {
        await request('/api/resaveGoodsInSupplierOrder', 'POST', { ...formGoods, id: currentPosition })
        setGoodsInSupplierOrderImport(prev => {
          let idx = prev.findIndex(el => +el.id === +currentPosition)
          if (idx >= 0) {
            prev[idx] = { ...formGoods, id: currentPosition }
          }
          return prev
        })
      } catch (error) {
        console.log(error)
      }
      // closeModalEdit()
    }
  



  useEffect(() => {
    (async () => {
      await getAllGoodsInSupplierOrderImport()
      // await getAllGoodsNameAndType()
    })()

  }, [])


  async function addGoodsInSupplierOrderImport() {
    try {
      setGoodsInSupplierOrderImport(await request('/api/addPositionInOrder', 'POST', { ...formGoods, props }))
    } catch (error) {
      console.log(error)
    }
    // closeModalAdd()
  }

  async function getAllGoodsInSupplierOrderImport() {
    try {
      const data = await request('/api/getAllGoodsInSupplierOrder', 'POST', props)
      setGoodsInSupplierOrderImport(data.allGoods)
    } catch (error) {
      setGoodsInSupplierOrderImport([])
    }
  }



  



  const changeHandler = e => {
    setFormGoods(prev => ({ ...prev, [e.target.name]: e.target.value }))
  }






  const subHeaderComponentMemo = React.useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText('');
      }
    };

    return (
      <FilterComponent onFilter={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} />
    );
  }, [filterText, resetPaginationToggle]);


  Array.prototype.sum = function (purchasePrice) {
    let total = 0
    for (let i = 0, _len = this.length; i < _len; i++) {
      total = total + +this[i][purchasePrice]
    }
    return total
  }

  const [name, setGoodsName] = useState(null)
  const [type, setGoodsType] = useState(null)

  async function getAllGoodsNameAndType() {
    console.log("getAllGoodsNameAndType?")
    try {
      const data = await request('/api/getAllGoodsNameAndType', 'GET')
      console.log("getAllGoodsNameAndType-", data.goodsCategoryResultArr)
      setGoodsName("goodsNameAndType-", data.goodsCategoryResultArr)
      setGoodsType("goodsNameAndType-", data.goodsCategoryResultArr)
    } catch (error) {
      setGoodsName([])
      setGoodsType([])
    }
  }

  // async function getAllGoodsCategory() {
  //   console.log("getAllGoodsCategory?")
  //   try {
  //     const data = await request('/api/getAllGoodsCategory', 'GET')
  //     console.log("getAllGoodsCategory-", data)
  //     setGoodsCategory(data.goodsCategoryResultArr)
  //   } catch (error) {
  //     setGoodsCategory([])
  //   }
  // }



  if (!goodsInSupplierOrderImport) {

    return (
      <><div id="preloader">
        <div id="loader"></div>
      </div></>
    )
  }

  return (
    <>
      <h4>Содержиммое заказа поставщику</h4>



      <div className="row align-items-center">
        <div className="col">
          <span>Товаров в заказе: {goodsInSupplierOrderImport.length}</span>
        </div>
        <div className="col">
          <span>На сумму: {goodsInSupplierOrderImport.sum("purchasePrice")}</span>
        </div>
        <div className="col d-flex justify-content-end">
          <button type="button" className="btn btn-success btn-sm" onClick={() => setHide(prev => !prev)}>Добавить</button>
        </div>
      </div>


      <div className={`${hide ? "hide-lightbox" : "lightbox"}`} style={{ background: "#E6E6FA" }}>
        <h5>Добавление позиции в заказ поставщику</h5>
        <table>
          <tr>
            <td>Наименование</td>
            <td>Сорт</td>
            <td>Стандарт поставки</td>
            <td>Ед. изм.</td>
            <td>Кол-во в уп.</td>
            <td>Цена закупки</td>
            <td>Кол-во заказано</td>
            <td>Цена продажи</td>
            <td>Статус</td>
            <td>Время поставки</td>
            <td>Наценка, %</td>
            <td>Сумма</td>
          </tr>

          <tr>
            <td>
              <Select
                onChange={changeHandler}
                className="select"
                options={name}
                isSearchable="true"
                styles={customStylesSelect}
                name="name"
              />
            </td>
            <td>
              <Select
                onChange={changeHandler}
                className="select"
                options={type}
                isSearchable="true"
                styles={customStylesSelect}
                name="type"
              />
            </td>
            <td>
              <InputGroup >
                <Form.Control onChange={changeHandler} name="supplyStandart" />
              </InputGroup>
            </td>
            <td>

              <select className="form-select" aria-label="Default select example" name="unit"
                onChange={changeHandler}
              >
                <option value="" selected>
                  Не выбрано.
                </option>
                {Object.values(units).map((unit) => (
                  <option value={unit}>{unit}</option>
                ))}
              </select>

              {/* <Select onChange={changeHandler} name="unit" className="select" options={options} isSearchable="true" placeholder={'Ед.изм.'} styles={customStylesSelect} /> */}
            </td>
            <td>
              <InputGroup >
                <Form.Control onChange={changeHandler} name="countInPack" />
              </InputGroup>
            </td>
            <td>
              <InputGroup >
                <Form.Control onChange={changeHandler} name="purchasePrice" />
              </InputGroup>
            </td>
            <td>
              <InputGroup >
                <Form.Control onChange={changeHandler} name="ordered" />
              </InputGroup>
            </td>
            <td>
              <InputGroup >
                <Form.Control onChange={changeHandler} name="sellPrice" />
              </InputGroup>
            </td>
            <td>
              <select className="form-select" aria-label="Default select example" name="status"
                onChange={changeHandler}
              >
                <option value="" selected>
                  Не выбрано.
                </option>
                {Object.values(itemStatus).map((status) => (
                  <option value={status}>{status}</option>
                ))}
              </select>

              {/* <Select onChange={changeHandler} name="status" className="select" options={options} isSearchable="true" placeholder={'Статус'} styles={customStylesSelect} /> */}
            </td>
            <td>
              <select className="form-select"
                name="deliveryTime"
                onChange={changeHandler}
              >
                <option value="" selected>
                  Не выбрано.
                </option>
                {Object.values(deliveryTime).map((deliveryTime) => (
                  <option value={deliveryTime}>{deliveryTime}</option>
                ))}
              </select>

              {/* <Select onChange={changeHandler} name="deliveryTime"  className="select" options={options} isSearchable="true" placeholder={'Время'} styles={customStylesSelect} /> */}
            </td>
            <td>
              <InputGroup >
                <Form.Control onChange={changeHandler} name="sum" className="select" />
              </InputGroup>
            </td>
            <td>
              <InputGroup >
                <Form.Control onChange={changeHandler} name="markup" className="select" />
              </InputGroup>
            </td>
          </tr>
        </table>
        <div className="d-flex justify-content-end">
          <button type="button" className="btn btn-success btn-sm" onClick={addGoodsInSupplierOrderImport}>Добавить в базу </button>
          <button type="button" className="btn btn-danger btn-sm" onClick={() => setHide(prev => !prev)}>Отмена</button>
        </div>
      </div>


      {/*  */}


      <DataTable
        onRowClicked={clickRow}
        columns={columnsGoods}
        data={goodsInSupplierOrderImport}
        pagination
        paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
        subHeader
        fixedHeader={true}
        fixedHeaderScrollHeight={"500px"}
        subHeaderComponent={subHeaderComponentMemo}
        persistTableHead
        responsive={true}
        expandableRows
        expandableRowsComponent={ExpandedComponent}
        expandOnRowClicked
        expandableRowsHideExpander
        highlightOnHover
      />
    </>
  );
};
export default {
  title: 'GoodsImport',
  component: GoodsImport,
};
import React from "react";
import DataTable from "react-data-table-component";
import { data } from "./data_move";
import "./table.css";


const Table = (props) => {
  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "30px",
      compact: true,
      cell: (row) => <div>{row.id}</div>
    },

    {
      name: "Дата",
      selector: "date",
      sortable: true,
      compact: true,

    },
    {
      name: "Кто",
      selector: "who",
      sortable: true,
      compact: true,
    },
    {
      name: "Кол-во",
      selector: "count",
      sortable: true,
      compact: true,
      width: "80px",
    },
    {
      name: "Со склада",
      selector: "from",
      sortable: true,
      compact: true,
    },
    {
      name: "На склад",
      selector: "to",
      sortable: true,
      compact: true,
    },
    {
      name: "Причина",
      selector: "reason",
      sortable: true,
      compact: true,
    },
    {
      name: "Комментарий",
      selector: "comment",
      sortable: true,
      compact: true,
      width: "250px",
    },
  ];
  return (
    <>
      <DataTable
        columns={columns}
        data={data}
        highlightOnHover
      />
    </>
  );
};
export default Table;

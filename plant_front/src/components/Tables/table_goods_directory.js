// import React from "react";
// import DataTable from "react-data-table-component";
// import { data } from "./data_goods_directory";
// import "./table.css";
// import { useState } from "react";
// import {
//   Button,
//   Col,
//   Container,
//   Form,
//   FormGroup,
//   Modal,
//   Row
// } from "react-bootstrap";


// const Table = (props) => {

//   const [show, setShow] = useState(false);
//   const closeModal = () => setShow(false);
//   const showModal = (row, event) => {
//     let myJSON = JSON.stringify(row);
//     setSomedata(myJSON)
//     setShow(true)
//     console.log("row- ", row);
//     setActiveRow(row);

//   }
//   const [somedata, setSomedata] = useState(null);

//   // const onRowClicked = (row, event) => {
//   //   console.log("row", row);
//   //   setActiveRow(row);
//   // };

//   const { setActiveRow } = props;
//   const columns = [
//     {
//       name: "ID",
//       selector: "id",
//       sortable: true,
//       width: "10px",
//       cell: (row) => <div>{row.id}</div>
//     },
//     {
//       name: "Категория",
//       selector: "category",
//       sortable: true,

//     },
//     {
//       name: "Название",
//       selector: "name",
//       sortable: true
//     },
//     {
//       name: "Сорт",
//       selector: "type",
//       sortable: true
//     },
//     {
//       name: "Единицы изм.",
//       selector: "unit",
//       sortable: true
//     },
//     {
//       name: "Сезон",
//       selector: "season",
//       sortable: true
//     },

//     {
//       name: "Стандарт",
//       selector: "standart",
//       sortable: true
//     },
//     {
//       name: "Последний поставщик",
//       selector: "lastsupplier",
//       sortable: true
//     },
//     {
//       name: "Последняя цена закупки",
//       selector: "lastpraice",
//       sortable: true
//     },
//     {
//       name: "% наценки",
//       selector: "markup",
//       sortable: true
//     },
//     {
//       name: "Производитель",
//       selector: "manufacturer",
//       sortable: true
//     },
//     {
//       name: "Штрихкод",
//       selector: "barcode",
//       sortable: true
//     },

//   ];



//   return (
//     <>
//       <DataTable
//         onRowClicked={showModal}
//         columns={columns}
//         data={data}
//         highlightOnHover
//       />

//       <Modal centered bsSize="large" show={show} onHide={closeModal}>
//         <Modal.Header>
//           <Modal.Title>Данные клиента</Modal.Title>
//           <Button variant="light" onClick={closeModal}>
//             X
//           </Button>
//         </Modal.Header>
//         <Modal.Body>

//           <table className="editClient">
//             <tr>
//               <td><strong>Вес, кг</strong></td>
//               <td><div contentEditable>0</div></td>
//               </tr>
//               <tr>
//               <td><strong>Длина, см</strong></td>
//               <td><div contentEditable>0</div></td>
//               </tr>
//               <tr>
//               <td><strong>Ширина, см</strong></td>
//               <td><div contentEditable>0</div></td>
//               </tr>
//               <tr>
//               <td><strong>Высота, см</strong></td>
//               <td><div contentEditable>0</div></td>
//               </tr>
//               <tr>
//               <td><strong>Описание:</strong></td>
//               <td><div contentEditable>Описание</div></td>
//               </tr>
//               <tr>
//               <td><strong>Стандарт поставки:</strong></td>
//               <td><div contentEditable>3 шт</div></td>
//               </tr>

//           </table>

          
//         </Modal.Body>
//         <Modal.Footer>
//           <Button variant="success">
//             Сохранить
//           </Button>
//           <Button variant="danger" >
//             Удалить
//           </Button>
//         </Modal.Footer>
//       </Modal>






//     </>

//   );
// };

// export default Table;

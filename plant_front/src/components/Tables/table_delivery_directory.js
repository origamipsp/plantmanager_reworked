// import React, { useEffect } from "react";
// import DataTable from "react-data-table-component";
//  import "./table.css";
// import { useHttp } from "../../hooks/http.hook"
// import { useState } from "react";
// import SimpleMenu from '../menu/menu';
// import {
//   Button,
//   Modal,
//   InputGroup,
//   Form,
// } from "react-bootstrap";

// const Table = (props) => {

//   const { request } = useHttp()
//   const [deliveries, setDeliveries] = useState(null)
//   const [currentCompany, setCurrentCompany] = useState(null)
//   const [form, setForm] = useState({
//     company: "",
//     tariff: "",
//     price: "",
//   })
//   const [show, setShow] = useState(false);
//   const closeModal = () => setShow(false);
//   const showModal = (row, event) => {

//     setForm(
//       {
//         company: row.company,
//         tariff: row.tariff,
//         price: row.price,
//       }
//     )
//     setShow(true)
//     setCurrentCompany(row.id)

//   }

//   const changeHandler = e => {
//     setForm(prev => ({ ...prev, [e.target.name]: e.target.value }))
//   }


//   async function getAllDelivery() {
//     try {
//       const data = await request('/api/getAllDelivery', 'GET')
//       setDeliveries(data.deliveries)
//     } catch (error) {
//       setDeliveries([])
//     }
//   }

//   useEffect(() => {
//     (async () => {
//       await getAllDelivery()
//     })()
//   }, [])


//   async function saveClick() {
//     try {
//       await request('/api/resaveDelivery', 'POST', { ...form, id: currentCompany })

//       setDeliveries(prev => {
//         let idx = prev.findIndex(el => +el.id === +currentCompany)
//         if (idx >= 0) {
//           prev[idx] = { ...form, id: currentCompany }
//         }
//         return prev
//       })
//     } catch (error) {
//       console.log(error)
//     }
//     closeModal()
//   }

//   // useEffect(() => {
//   //   return () => { }
//   // }, [])

//   // useEffect(() => { return () => { }}, [])

//   const columns = [
//     {
//       name: "#",
//       selector: "id",
//       sortable: true,
//       width: "60px",
//       cell: (row) => <div>{row.id}</div>
//     },
//     {
//       name: "Компания",
//       selector: "company",
//       sortable: true,

//     },
//     {
//       name: "Тариф",
//       selector: "tariff",
//       sortable: true
//     },
//     {
//       name: "Стоимость, руб/кг",
//       selector: "price",
//       sortable: true
//     },
//     {
//       name: "Действие",
//       cell: row => <SimpleMenu size="small" row={row} />,
//       allowOverflow: true,
//       button: true,
//       compact: true,
//     },
//   ];

//   if (!deliveries?.length) {
//     return (
//       <></> //loader
//     )
//   }

//   return (
//     <>
//       <DataTable
//         onRowClicked={showModal}
//         columns={columns}
//         data={deliveries}
//         highlightOnHover
//         pointerOnHover
//       />

//       <Modal centered bsSize="large" show={show} onHide={closeModal}>
//         <Modal.Header>
//           <Modal.Title>Изменить "Доставку"</Modal.Title>
//           <Button variant="light" onClick={closeModal}>
//             X
//           </Button>
//         </Modal.Header>
//         <Modal.Body>
//           <InputGroup size="sm" className="mb-3">
//             <InputGroup.Text className="inputNameWidth" >Компания</InputGroup.Text>
//             <Form.Control
//               value={form.company}
//               onChange={changeHandler}
//               name="company"
//             />
//           </InputGroup>
//           <InputGroup size="sm" className="mb-3">
//             <InputGroup.Text className="inputNameWidth" >Тариф</InputGroup.Text>
//             <Form.Control
//               value={form.tariff}
//               onChange={changeHandler}
//               name="tariff"
//             />
//           </InputGroup>
//           <InputGroup size="sm" className="mb-3">
//             <InputGroup.Text className="inputNameWidth" >Цена, руб/кг</InputGroup.Text>
//             <Form.Control
//               type="number"
//               value={form.price}
//               onChange={changeHandler}
//               name="price"
//             />
//           </InputGroup>
//         </Modal.Body>
//         <Modal.Footer>
//           <Button variant="success" onClick={saveClick}>
//             Сохранить
//           </Button>
//         </Modal.Footer>
//       </Modal>
//     </>
//   );
// };

// export default Table;

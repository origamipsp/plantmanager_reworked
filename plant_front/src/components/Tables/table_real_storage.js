import React from "react";
import DataTable from "react-data-table-component";
import { data } from "./data_real_storage";
import "./table.css";
import { useState } from "react";
import 'react-select-search/style.css'
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import TableMove from "../Tables/table_move";


import {
  Button,
  Modal,
} from "react-bootstrap";





const customStyles = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,

  }),
  container: (provided, state) => ({
    ...provided,

    width: 120,
  }),
}


const customStyles2 = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,

  }),
  container: (provided, state) => ({
    ...provided,

    width: 120,
  }),
}

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];


const Table = (props) => {

  const [show, setShow] = useState(false);
  const closeModal = () => setShow(false);
  const showModal = (row, event) => {
    let myJSON = JSON.stringify(row);
    setSomedata(myJSON)
    setShow(true)
    console.log("row- ", row);
    setActiveRow(row);
  }


  const [somedata, setSomedata] = useState(null);



  const { setActiveRow } = props;
  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "30px",
      compact: true,
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Категория",
      selector: "category",
      sortable: true,
      compact: true,

    },
    {
      name: "Название",
      selector: "name",
      sortable: true,
      compact: true,
    },
    {
      name: "Сорт",
      selector: "type",
      sortable: true,
      compact: true,
    },
    {
      name: "Стандарт",
      selector: "standart",
      sortable: true,
      compact: true,
    },
    {
      name: "Поставщик",
      selector: "supplier",
      sortable: true,
      compact: true,
    },

    {
      name: "Сезон",
      selector: "season",
      sortable: true,
      compact: true,
    },

    {
      name: "Факт приход",
      selector: "f_income",
      sortable: true,
      compact: true,
    },
    {
      name: "Факт цена",
      selector: "f_price",
      sortable: true,
      compact: true,
    },
    {
      name: "Продано предзак.",
      selector: "preOrder",
      sortable: true,
      compact: true,
    },
    {
      name: "Продано",
      selector: "sold",
      sortable: true,
      compact: true,
    },
    {
      name: "Розница",
      selector: "retail",
      sortable: true,
      compact: true,
    },
    {
      name: "Списано",
      selector: "writeOff",
      sortable: true,
      compact: true,
    },
    {
      name: "Остаток",
      selector: "remainder",
      sortable: true,
      compact: true,
    },
  ];

  return (
    <>

      <div className="selectWrap">
      <input type="text" style={{width: "150px"}} id="inputPassword6" className="form-control" aria-describedby="passwordHelpInline"  placeholder="Категория"></input>
      <input type="text" style={{width: "150px"}}  id="inputPassword6" className="form-control" aria-describedby="passwordHelpInline"  placeholder="Название"></input>
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Поставщик'}
          styles={customStyles}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Сезон'}
          styles={customStyles2}
        />

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Остатки'}
          styles={customStyles}
        />


        <div className="form-group row">

          <div className="col-xs-10">
            <input className="form-control" type="date" id="example-date-input"></input>
          </div>
        </div>


        <div className="form-group row">

          <div className="col-xs-10">
            <input className="form-control" type="date" id="example-date-input"></input>
          </div>
        </div>

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Остатки'}
          styles={customStyles}
        />
        <button onClick={showModal} type="button" class="btn btn-outline-success">Фильтровать</button>
        <button onClick={showModal} type="button" class="btn btn-outline-danger">Очистить</button>
        <button onClick={showModal} type="button" class="btn btn-outline-success">Excel</button>


      </div>

      <DataTable
        onRowClicked={showModal}
        columns={columns}
        data={data}
        highlightOnHover
      />




      <Modal size="xl" scrollable
        aria-labelledby="contained-modal-title-vcenter"
        centered show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Перемещения товара роза по складам</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody">

          <TableMove setActiveRow={setActiveRow} />
        </Modal.Body>


        <Modal.Footer>

          <Button variant="warning" onClick={closeModal} >
            Закрыть
          </Button>
        </Modal.Footer>
      </Modal>

    </>

  );
}; 

export default Table;

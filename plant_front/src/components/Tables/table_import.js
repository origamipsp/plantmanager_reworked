import React, { useRef, useEffect } from "react";
import DataTable from "react-data-table-component";
import { data } from "./data_import";
import { dataGoods } from "./data_goods";
import { dataPayments } from "./data_payments";
import "./table.css";
import { useState } from "react";
import SimpleMenu from '../menu/menu';
import Select from 'react-select';
import { seasonality } from '../../data/select_config_data';
import { customerOrderStatus } from '../../data/select_config_data';
import { useHttp } from "../../hooks/http.hook"
import { GoodsImport } from "./table_import_goods";
import InputGroup from 'react-bootstrap/InputGroup';
import {
  Button,
  Form,
  Modal,
} from "react-bootstrap";


const FilterComponent = ({ filterText, onFilter, onClear }) => (
  <>
    <input
      id="search"
      type="text"
      placeholder="Фильтр по наименованию"
      aria-label="Search Input"
      value={filterText}
      onChange={onFilter}
    ></input>
    <Button type="button" onClick={onClear}>
      X
    </Button>
  </>
);

const CustomMenu = React.forwardRef(
  ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
    const [value, setValue] = useState('');
    return (
      <div
        ref={ref}
        style={style}
        className={className}
        aria-labelledby={labeledBy}
      >
        <Form.Control
          autoFocus
          className="mx-3 my-2 w-auto"
          placeholder="Type to filter..."
          onChange={(e) => setValue(e.target.value)}
          value={value}
        />
        <ul className="list-unstyled">
          {React.Children.toArray(children).filter(
            (child) =>
              !value || child.props.children.toLowerCase().startsWith(value),
          )}
        </ul>
      </div>
    );
  },
);


const customStylesSelect = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,

  }),
  container: (provided, state) => ({
    ...provided,

    width: 250,
  }),
}


const Table = (props) => {
  const { request } = useHttp()
  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "60px",
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      compact: true,

    },
    {
      name: "Заголовок",
      selector: "title",
      sortable: true,
      compact: true,
    },
    {
      name: "Поставщик",
      selector: "supplier",
      sortable: true,
      compact: true,
    },
    {
      name: "Сезон",
      selector: "season",
      sortable: true,
      compact: true,
    },
    {
      name: "Сумма",
      selector: "sum",
      sortable: true,
      compact: true,
    },
    {
      name: "Оплата",
      selector: "payment",
      sortable: true,
      compact: true,
    },
    {
      name: "Статус",
      selector: "status",
      sortable: true,
      compact: true,
    },
    // {
    //   name: "Действие",
    //   cell: row => <SimpleMenu size="small" row={row} menuItems={[{ title: ["Изменить"], onClick: () => { console.log("123qwe") } }]} />,
    //   allowOverflow: true,
    //   button: true,
    //   compact: true,
    // },
  ];
  const columnsPayments = [
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      compact: true,
    },
    {
      name: "Назначение",
      selector: "purpose",
      sortable: true,
      compact: true,
    },
    {
      name: "	Сумма",
      selector: "sum",
      sortable: true,
      compact: true,
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,
      compact: true,
    },
  ];

  const [showImportAdd, setShowImportAdd] = useState(false);
  const closeModalImportAdd = () => setShowImportAdd(false);
  const showModalImportAdd = (row, event) => {
    setShowImportAdd(true)
  }

  const [showImportEdit, setShowImportEdit] = useState(false);
  const closeModalEdit = () => setShowImportEdit(false);
  const showModalEdit = (row, event) => {
    setFormImport({
      id: row.id,
      date: row.date,
      title: row.title,
      supplier: row.supplier,
      season: row.season,
      sum: row.sum,
      payment: row.payment,
      status: row.status,
      deliveryPrice: row.deliveryPrice,
      packPrice: row.packPrice,
      customsPrice: row.customsPrice,
    })
    setShowImportEdit(true)
    setCurrentImportedOrder(row.id)
    setRow(row)
    setActiveRow(row);
  }

  const [formImport, setFormImport] = useState({
    id: 0,
    date: "",
    title: "",
    supplier: "",
    season: "",
    sum: 0,
    payment: 0,
    status: "",
    deliveryPrice: 0,
    packPrice: 0,
    customsPrice: 0,
  })

  const [importedOrders, setImportedOrders] = useState(null)
  const [currentImportedOrder, setCurrentImportedOrder] = useState(null)

  const [row, setRow] = useState(null);

  const changeHandler = e => {
    setFormImport(prev => ({ ...prev, [e.target.name]: e.target.value }))
  }

  const [suppliersForSelect, setSuppliersForSelect] = useState(null)

  const { setActiveRow } = props;

  const [filterText, setFilterText] = React.useState('');
  const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
  const filteredItems = dataGoods.filter(
    item => item.name && item.name.toLowerCase().includes(filterText.toLowerCase()),
  );

  const subHeaderComponentMemo = React.useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText('');
      }
    };

    return (
      <FilterComponent onFilter={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} />
    );
  }, [filterText, resetPaginationToggle]);

  const [hideGoods, setHideGoods] = useState(true);
  const [hidePayments, setHidePayments] = useState(true);
  const [hideAddPayments, setHideAddPayments] = useState(true);


  useEffect(() => {
    (async () => {
      await getAllSuppliersForSelect().then(await getAllImportedOrders())

    })()
  }, [])

  async function getAllSuppliersForSelect() {
    try {
      const data = await request('/api/getAllSuppliersForSelect', 'GET')
      console.log("getAllSuppliersForSelect-", data.result)
      setSuppliersForSelect(data.result)
    } catch (error) {
      setSuppliersForSelect({})
    }
  }



  async function getAllImportedOrders() {
    try {
      const data = await request('/api/getAllImportedOrders', 'GET')
      console.log("getAllImportedOrders-", data.sup)
      setImportedOrders(data.sup)
    } catch (error) {
      setImportedOrders({})
    }
  }

  async function addClick() {
    try {
      setImportedOrders(await request('/api/addImportedOrders', 'POST', { ...formImport }))
    } catch (error) {
      console.log(error)
    }
    closeModalImportAdd()
  }

  async function saveClick() {
    console.log("form-", { ...formImport, id: currentImportedOrder })
    try {
      await request('/api/resaveImportedOrders', 'POST', { ...formImport, id: currentImportedOrder })
      setImportedOrders(prev => {
        let idx = prev.findIndex(el => +el.id === +currentImportedOrder)
        if (idx >= 0) {
          prev[idx] = { ...formImport, id: currentImportedOrder }
        }
        return prev
      })
    } catch (error) {
      console.log(error)
    }
    closeModalEdit()
  }






  const [season, setSeason] = useState("");
  const [supplier, setSupplier] = useState("");
  const [status, setStatus] = useState("");

  const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
  ];
  // !suppliersForSelect && 


  Array.prototype.sum = function (field, inWork) {
    let total = 0
    let count = 0
    if (inWork) {
      for (let i = 0, length = this.length; i < length; i++) {
        if (this[i]['status'] == "в работе") {
          total = total + +this[i][field]
          count++
        }
      }
      return { total: total, count: count }
    } else {
      for (let i = 0, length = this.length; i < length; i++) {
        total = total + +this[i][field]
      }
      return { total: total }
    }
  }


  if (!importedOrders) {
    return (
      <><div id="preloader">
        <div id="loader"></div>
      </div></>
    )
  }
  return (
    <>
      <p>Всего заказов <strong>в работе</strong>: {importedOrders.sum("sum", true).count}, на сумму: {importedOrders.sum("sum", true).total}</p>
      <p>Всего заказов: {importedOrders.length}, на сумму: {importedOrders.sum("sum").total}, оплат на сумму: {importedOrders.sum("payment").total}</p>
      <button type="button" className="btn btn-success" onClick={showModalImportAdd}>Добавить</button>


      <DataTable
        pointerOnHover={true}
        onRowClicked={showModalEdit}
        columns={columns}
        data={importedOrders}
        highlightOnHover
      />

      <Modal size="xl" fullscreen show={showImportEdit} onHide={closeModalEdit}>
        <Modal.Header>
          <Modal.Title>Просмотр/изменение заказа поставщику</Modal.Title>
          <Button variant="light" onClick={closeModalEdit}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>
          <table style={{ width: "100%", background: "#E6E6FA" }}>
            <tr>
              <td>#</td>
              <td>Дата</td>
              <td>Заголовок</td>
              <td>Поставщик</td>
              <td>Сезон</td>
              <td>Сумма</td>
              <td>Оплата</td>
              <td>Статус</td>
            </tr>
            <tr>
              <td>
                {row && row.id ? row.id : ""}
              </td>
              <td><div className="form-group row">
                <div className="col-xs-10">
                  <input className="form-control" type="date"
                    value={formImport && formImport.date ? formImport.date : ""}
                    name="date"
                    onChange={changeHandler}
                  ></input>
                </div>
              </div></td>
              <td>
                <InputGroup >
                  <Form.Control
                    value={formImport.title}
                    onChange={changeHandler}
                    name="title"
                  />
                </InputGroup>
              </td>
              <td>
                <select className="form-select" aria-label="Default select example" name="supplier"
                  onChange={changeHandler}
                >
                  <option value="" selected>
                    {formImport && formImport.supplier ? formImport.supplier : ""}
                  </option>
                  {Object.values(suppliersForSelect).map((supplier) => (
                    <option value={supplier}>{supplier}</option>
                  ))}
                </select>
              </td>
              <td>
                <select className="form-select" aria-label="Default select example"
                  onChange={changeHandler}
                  name="season"
                >
                  <option value="" selected>
                    {formImport && formImport.season ? formImport.season : ""}
                  </option>
                  {Object.values(seasonality).map((season) => (
                    <option value={season}>{season}</option>
                  ))}
                </select>
              </td>
              <td>{formImport && formImport.sum ? formImport.sum : ""}</td>
              <td>{formImport && formImport.payment ? formImport.payment : ""}</td>
              <td>
                <select className="form-select"
                  name="status"
                  onChange={changeHandler}
                >
                  <option value="" selected>
                    {formImport && formImport.status ? formImport.status : ""}
                  </option>
                  {Object.values(customerOrderStatus).map((status) => (
                    <option value={status}>{status}</option>
                  ))}
                </select>
              </td>
            </tr>
          </table>
          {/* <div className="" style={{ background: "#E6E6FA" }}>
            <div className="row ">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text id="basic-addon3">Стоимость доставки:</InputGroup.Text>
                  <Form.Control
                    value={formImport.deliveryPrice}
                    onChange={changeHandler}
                    name="deliveryPrice"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text id="basic-addon4">Стоимость упаковки:</InputGroup.Text>
                  <Form.Control
                    value={formImport.packPrice}
                    onChange={changeHandler}
                    name="packPrice"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text id="basic-addon5">Стоимость таможни:</InputGroup.Text>
                  <Form.Control
                    value={formImport.customsPrice}
                    onChange={changeHandler}
                    name="customsPrice"
                  />
                </InputGroup>
              </div> */}
          <div className="d-flex justify-content-end">
            <button type="button" class="btn btn-warning btn-sm" onClick={saveClick}>Изменить</button>
          </div>
          {/* </div> */}
          {/* </div> */}
          <GoodsImport id={formImport.id} />
          <div className={`${hidePayments ? "hide-lightbox" : "lightbox"}`} >
            <button type="button" className="btn btn-outline-success" onClick={() => setHideAddPayments(prev => !prev)}>Добавить оплату</button>
            <p>Оплат: 0 На сумму: 0</p>
            <div className={`${hideAddPayments ? "hide-lightbox" : "lightbox"}`}>
              <table>
                <tr>
                  <td>Дата</td>
                  <td>Назначение</td>
                  <td>Сумма</td>
                </tr>
              </table>
            </div>
            <DataTable
              columns={columnsPayments}
              data={dataPayments}
              highlightOnHover
            />
          </div>
        </Modal.Body>
      </Modal>


      <Modal centered size="xl" show={showImportAdd} onHide={closeModalImportAdd}>
        <Modal.Header>
          <Modal.Title>Добавить заказ поставщику</Modal.Title>
          <Button variant="light" onClick={closeModalImportAdd}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>
          <table width={1100}>
            <tr>
              <td>Дата</td>
              <td>Заголовок</td>
              <td>Поставщик</td>
              <td>Сезон</td>
              <td>Сумма</td>
              <td>Оплата</td>
              <td>Статус</td>
            </tr>
            <tr>
              <td><div className="form-group row">
                <div className="col-xs-10">
                  <input className="form-control" type="date"
                    id="example-date-input" onChange={changeHandler}
                    name="date">
                  </input>
                </div>
              </div></td>
              <td>
                <InputGroup >
                  <Form.Control onChange={changeHandler} name="title" />
                </InputGroup>
              </td>
              <td>
                <select className="form-select"
                  name="supplier"
                  onChange={changeHandler}
                >
                  <option selected disabled>
                    Не выбрано.
                  </option>
                  {Object.values(suppliersForSelect).map((supplier) => (
                    <option value={supplier}>{supplier}</option>
                  ))}
                </select>
              </td>
              <td>
                <select className="form-select"
                  name="season"
                  onChange={changeHandler}
                >
                  <option value="" selected disabled>
                    Не выбрано.
                  </option>
                  {Object.values(seasonality).map((season) => (<option value={season}>{season} </option>))}
                </select>
              </td>
              <td>
                <InputGroup >
                  <Form.Control disabled name="sum" style={{ width: "70px" }} />
                </InputGroup>
              </td>
              <td>
                <InputGroup >
                  <Form.Control disabled name="payment" style={{ width: "70px" }} />
                </InputGroup>
              </td>
              <td>
                <select className="form-select"
                  name="status"
                  onChange={changeHandler}
                >
                  <option selected disabled>
                    Не выбрано.
                  </option>
                  {Object.values(customerOrderStatus).map((status) => (
                    <option value={status}>{status}</option>
                  ))}
                </select>
              </td>
            </tr>
          </table>
          {/* <div className="container text-center">
            <div className="row ">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text id="basic-addon3">Стоимость доставки:</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="deliveryPrice"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text id="basic-addon4">Стоимость упаковки:</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="packPrice"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text id="basic-addon5">Стоимость таможни:</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="customsPrice"
                  />
                </InputGroup>
              </div>
            </div>
          </div> */}
          <Modal.Footer>
            <Button variant="success" onClick={addClick}>
              Сохранить
            </Button>
            <Button variant="danger" onClick={closeModalImportAdd}>
              Отмена
            </Button>
          </Modal.Footer>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default Table;

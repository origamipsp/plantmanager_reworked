//mock data
export const data = [
    {
      id: "1",
      date: "2011-08-19",
      client: "Стайловская Наталия Федоровна",
      nickName: "Наталия цветы",
      origin: "одноклассники",
      season: "всегда",
      sum: "7550",
      payment: "16 079",
      paymentType: "б/нал.",
      delivery: "ТК Сдэк 0",
      status: "завершён",
    },

    {
      id: "2",
      date: "2011-08-19",
      client: "Маша три рубля и наша",
      nickName: "Маша 3 рубля",
      origin: "одноклассники",
      season: "всегда",
      sum: "7550",
      payment: "16 079",
      paymentType: "б/нал.",
      delivery: "ТК Сдэк 0",
      status: "завершён",
    },

    {
      id: "3",
      date: "2011-08-19",
      client: "Зина из магазина",
      nickName: "Зина магазин",
      origin: "одноклассники",
      season: "всегда",
      sum: "7550",
      payment: "16 079",
      paymentType: "б/нал.",
      delivery: "ТК Сдэк 0",
      status: "завершён",
    },
    

  ];
import React, { useEffect } from 'react';
import styled from 'styled-components';
import DataTable from "react-data-table-component";
import "./table.css";
import { dataGoods } from "./data_goods";
import Button from 'react-bootstrap/Button';
import SimpleMenu from '../menu/menu';
import { Line } from '../../components/Line/line'
import InputGroup from 'react-bootstrap/InputGroup';
import Dropdown from 'react-bootstrap/Dropdown'
import { data } from "./data_import";//
import { useState } from 'react';
import Select from 'react-select';
import { units } from '../../data/select_config_data';
import { deliveryTime } from '../../data/select_config_data';
import { useHttp } from "../../hooks/http.hook"

import {
  Form,
  FormGroup,
  Modal,
  Row
} from "react-bootstrap";





const customStylesSelect = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,
  }),
  container: (provided, state) => ({
    ...provided,
    zIndex: 2,
  }),
}

const customStyles = {
  headRow: {
    style: {
    }
  },
  rows: {
    style: {
    },
  },
  headCells: {
    style: {
      backgroundColor: '#E6E6FA',
    },
  },
  cells: {
    style: {
      backgroundColor: '#E6E6FA',
    },
  },
};

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];



const columnsGoods = [
  {
    name: "#",
    selector: "id",
    sortable: true,
    width: "100px",
    cell: (row) => <div>{row.id}</div>
  },
  {
    name: "Дата накладной",
    selector: "consignmentNotedate",
    sortable: true,
    compact: true,
    // width: "105px",

  },
  {
    name: "Номер накладной",
    selector: "consignmentNoteNumber",
    sortable: true,
    compact: true,
    // width: "65px",
  },
  {
    name: "Дата поставки",
    selector: "supplyDate",
    sortable: true,
    compact: true,
    // width: "75px",
  },
  {
    name: "Поставлено",
    selector: "delivered",
    sortable: true,
    compact: true,
    // width: "65px",
  },
  {
    name: "Сумма",
    selector: "sum",
    sortable: true,
    compact: true,
    // width: "55px",
  },
  {
    name: "Действие",
    cell: row => <SimpleMenu size="small" row={row} menuItems={[{ title: ["Удалить"], onClick: () => { console.log("123qwe") } }]} />,
    allowOverflow: true,
    button: true,
    compact: true,
    // width: "55px",
  },
];

export const GoodsIncome = (props) => {
  const { request } = useHttp()

// console.log("333", props.id)
  const [filterText, setFilterText] = React.useState('');
  const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
  const filteredItems = dataGoods.filter(
    item => item.name && item.name.toLowerCase().includes(filterText.toLowerCase()),
  );

  const [unit, setUnit] = useState("");
  const [delTime, setDelTime] = useState("");
  const [row, setRow] = useState(null);
  const ExpandedComponent = ({ data }) => <pre>
    <>
      <h5>Изменение поставки по данной позиции в заказе поставщику</h5>
      <table>
        <tr>
          <td>Дата накладной</td>
          <td>Номер накладной</td>
          <td>Дата поставки</td>
          <td>Поставлено</td>
          <td>Сумма</td>
        </tr>





        

        <tr>
          <td>
          <input className="form-control" type="date"
                    // value={formImport && formImport.date ? formImport.date : ""}
                    name="date"
                    // onChange={changeHandler}
                  ></input>
          </td>
          <td>
            <InputGroup >
              <Form.Control />
            </InputGroup>
          </td>
          <td>
          <input className="form-control" type="date"
                    // value={formImport && formImport.date ? formImport.date : ""}
                    name="date"
                    // onChange={changeHandler}
                  ></input>
          </td>
          <td>
            <InputGroup >
              <Form.Control />
            </InputGroup>
          </td>
          <td>
            <InputGroup >
              <Form.Control />
            </InputGroup>
          </td>
          
          <td>
          <button type="button" className="btn btn-warning btn-sm" >Изменить</button> 
          </td>    

          
        </tr>
      </table>
      <div className="d-flex justify-content-end">
       
       
      </div>

    </>
  </pre>;






  const [hide, setHide] = useState(true);




  const [goodsIncome, setGoodsIncome] = useState(null)




  useEffect(() => {
    (async () => {
      await getAllGoodsIncome()
    })()
  }, [])

  async function getAllGoodsIncome() {
    try {
      const data = await request('/api/getAllGoodsIncome', 'POST', props)
      setGoodsIncome(data.allGoodsIncome)
    } catch (error) {
      setGoodsIncome([])
    }
  }


  if (!goodsIncome) {
    return (
      <><div id="preloader">
        <div id="loader"></div>
      </div></>
    )
  }

  return (
    <>

      <h5>Все поставки заказчика по данной позиции в заказе</h5>
      <div className="d-flex justify-content-end">
        <button type="button" className="btn btn-success btn-sm" onClick={() => setHide(prev => !prev)}>Добавить</button>
      </div>



      {/* <div className={`${hide ? "hide-lightbox" : "lightbox"}`} style={{ background: "#E6E6FA" }}>
        <h5>Добавление позиции в заказ поставщику2</h5>
        <table>
          <tr>
            <td>Наименование/Сорт</td>
            <td>Стандарт поставки</td>
            <td>Ед. изм.</td>
            <td>Кол-во в уп.</td>
            <td>Цена закупки</td>
            <td>Кол-во заказано</td>
            <td>Цена продажи</td>
            <td>Статус</td>
            <td>Время поставки</td>
            <td>Наценка, %</td>
            <td>Сумма</td>
          </tr>

          <tr>
            <td>
              <Select className="select" options={options} isSearchable="true" placeholder={'Наименование'} styles={customStylesSelect} />
            </td>
            <td>
              <InputGroup >
                <Form.Control />
              </InputGroup>
            </td>
            <td>
              <Select className="select" options={options} isSearchable="true" placeholder={'Ед.изм.'} styles={customStylesSelect} />
            </td>
            <td>
              <InputGroup >
                <Form.Control />
              </InputGroup>
            </td>
            <td>
              <InputGroup >
                <Form.Control />
              </InputGroup>
            </td>
            <td>
              <InputGroup >
                <Form.Control />
              </InputGroup>
            </td>
            <td>
              <InputGroup >
                <Form.Control />
              </InputGroup>
            </td>
            <td>
              <Select className="select" options={options} isSearchable="true" placeholder={'Статус'} styles={customStylesSelect} />
            </td>
            <td>
              <Select className="select" options={options} isSearchable="true" placeholder={'Время'} styles={customStylesSelect} />
            </td>
            <td>
              <InputGroup >
                <Form.Control />
              </InputGroup>
            </td>
            <td>
              <InputGroup >
                <Form.Control />
              </InputGroup>
            </td>
          </tr>
        </table>
        <div className="d-flex justify-content-end">
          <button type="button" className="btn btn-success btn-sm" >Сохранить</button>
          <button type="button" className="btn btn-danger btn-sm" onClick={() => setHide(prev => !prev)}>Отмена</button>
        </div>
      </div> */}




      <DataTable
        customStyles={customStyles}
        columns={columnsGoods}
         data={goodsIncome}
        pagination
        fixedHeader={true}
        persistTableHead
        responsive={true}
        expandableRows
        expandableRowsComponent={ExpandedComponent}
      />
      <h5>Следующая позиция в заказе поставщику</h5>
    </>
  );
};
export default {
  title: 'GoodsIncome',
  component: GoodsIncome,
};
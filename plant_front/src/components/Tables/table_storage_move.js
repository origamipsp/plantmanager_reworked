import React from "react";
import DataTable from "react-data-table-component";
import { data } from "./data_storage_move";
import { dataMove } from "./data_storage_move_move";
import "./table.css";
import { useState } from "react";
import 'react-select-search/style.css'
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import SimpleMenu from '../menu/menu';




const customStyles = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,

  }),
  container: (provided, state) => ({
    ...provided,

    width: 120,
  }),
}
const customStyles3 = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,

  }),
  container: (provided, state) => ({
    ...provided,

    width: 300,
  }),
}



const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];


const Table = (props) => {
  const ExpandedComponent = ({ data }) => <pre>

  <p>Перемещения товара роза по складам</p>

  <p>Товаров в документе: 1 Сумма закуп: 187 руб. Сумма розница 325 руб. Сумма маржа: 138 руб.</p>
  <button type="button" className="btn btn-success" onClick={() => setHide(prev => !prev)}>Добавить товар</button>


  <table className={`${hide ? "hide-lightbox" : "lightbox"}`}>
    <tr>
      <td>Название Сорт</td>
      <td>Стандарт</td>
      <td>Сезон</td>
      <td>Доступно</td>
      <td>Кол-во</td>
      <td>Цена, руб.</td>
      <td>Цена розн, руб.</td>
      <td>Сумма, руб.</td>
      <td>Сумма розн, руб.</td>
      <td>Маржа, руб.</td>
      <td  colspan="2">Действие</td>
    </tr>
    <tr>
      <td>
        <Select
          className="select"
          options={options}
          isSearchable="true"
          styles={customStyles3}

        />
      </td>
      <td><input type="text" id="inputPassword6" className="form-control" disabled></input></td>
      <td><input type="text" id="inputPassword6" className="form-control" disabled></input></td>
      <td><input type="text" id="inputPassword6" className="form-control"disabled></input></td>
      <td><input type="text" id="inputPassword6" className="form-control"></input></td>
      <td><input type="text" id="inputPassword6" className="form-control"disabled></input></td>
      <td><input type="text" id="inputPassword6" className="form-control"disabled></input></td>
      <td><input type="text" id="inputPassword6" className="form-control"disabled></input></td>
      <td><input type="text" id="inputPassword6" className="form-control"disabled></input></td>
      <td><input type="text" id="inputPassword6" className="form-control"disabled></input></td>
      <td>
        <button type="button" class="btn btn-success">Сохранить</button>
      </td>
      <td>
        <button type="button" class="btn btn-danger">Отмена</button>
      </td>
    </tr>
  </table>





  <DataTable
    columns={columnsMove}
    data={dataMove}
    highlightOnHover
  />


</pre >;
  const [hide, setHide] = useState(true);
  // const [show, setShow] = useState(false);
  // const closeModal = () => setShow(false);
  // const showModal = (row, event) => {
  //   let myJSON = JSON.stringify(row);
  //   setSomedata(myJSON)
  //   setShow(true)
  //   console.log("row- ", row);
  //   setActiveRow(row);
  // }


  // const [somedata, setSomedata] = useState(null);



  const { setActiveRow } = props;
  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "50px",
      compact: true,
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      compact: true,

    },
    {
      name: "Со склада",
      selector: "from",
      sortable: true,
      compact: true,
    },
    {
      name: "На склад",
      selector: "to",
      sortable: true,
      compact: true,
    },
    {
      name: "Причина",
      selector: "reason",
      sortable: true,
      compact: true,
    },
    {
      name: "Исполнитель",
      selector: "executor",
      sortable: true,
      compact: true,
    },

    {
      name: "Сезон",
      selector: "season",
      sortable: true,
      compact: true,
    },

    {
      name: "Сумма закуп",
      selector: "totalpurchase",
      sortable: true,
      compact: true,
    },
    {
      name: "Сумма розница",
      selector: "totalretail",
      sortable: true,
      compact: true,
    },
    {
      name: "Сумма маржа",
      selector: "totalmargin",
      sortable: true,
      compact: true,
    },

    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,

      compact: true,

    },
  ];

  const columnsMove = [

    {
      name: "Название",
      selector: "name",
      sortable: true,
      compact: true,


    },
    {
      name: "Сорт",
      selector: "type",
      sortable: true,
      compact: true,
     
    },
    {
      name: "Стандарт",
      selector: "standart",
      sortable: true,
      compact: true,
    
    },
    {
      name: "Сезон",
      selector: "season",
      sortable: true,
      compact: true,
    
    },
    {
      name: "Кол-во",
      selector: "count",
      sortable: true,
      compact: true,
   
    },

    {
      name: "Цена, руб.",
      selector: "price",
      sortable: true,
      compact: true,

    },

    {
      name: "Цена розн, руб.",
      selector: "priceRetail",
      sortable: true,
      compact: true,
 
    },
    {
      name: "Сумма, руб.",
      selector: "sum",
      sortable: true,
      compact: true,

    },
    {
      name: "Сумма розн, руб.",
      selector: "sumRetail",
      sortable: true,
      compact: true,

    },

    {
      name: "Маржа, руб.",
      selector: "margin",
      sortable: true,
      compact: true,
  
    },


  ];
  
  return (
    <>
      <br></br>
      <div className="selectWrap">

        <button type="button" class="btn btn-success">Добавить</button>

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'СоСклада'}
          styles={customStyles}
        />

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'НаСклад'}
          styles={customStyles}
        />

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Исполнитель'}
          styles={customStyles}
        />

        <input type="text" style={{ width: "150px" }} id="inputPassword6" className="form-control" placeholder="Коментарий"></input>

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Сезон'}
          styles={customStyles}
        />


        <div className="form-group row">

          <div className="col-xs-10">
            <input className="form-control" type="date" id="example-date-input"></input>
          </div>
        </div>


        <div className="form-group row">

          <div className="col-xs-10">
            <input className="form-control" type="date" id="example-date-input"></input>
          </div>
        </div>
        {/* <DatePicker selected={startDate} /> */}

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Остатки'}
          styles={customStyles}
        />
        <button  type="button" class="btn btn-outline-success">Фильтровать</button>
        <button type="button" class="btn btn-outline-danger">Очистить</button>
        <button type="button" class="btn btn-outline-success">Отчет</button>


      </div>

      <DataTable
        // onRowClicked={showModal}
        columns={columns}
        data={data}
        highlightOnHover
        expandableRows
        expandableRowsComponent={ExpandedComponent}
      />



{/* 
      <Modal size="xl" scrollable
        aria-labelledby="contained-modal-title-vcenter"
        centered show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Перемещения товара роза по складам</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody">
          <p>Товаров в документе: 1</p>
          <p>Сумма закуп: 187 руб.</p>
          <p>Сумма розница 325 руб.</p>
          <p>Сумма маржа: 138 руб.</p>
          <button type="button" className="btn btn-success" onClick={() => setHide(prev => !prev)}>Добавить товар</button>


          <table className={`${hide ? "hide-lightbox" : "lightbox"}`}>
            <tr>
              <td>Название Сорт</td>
              <td>Стандарт</td>
              <td>Сезон</td>
              <td>Доступно</td>
              <td>Кол-во</td>
              <td>Цена, руб.</td>
              <td>Цена розн, руб.</td>
              <td>Сумма, руб.</td>
              <td>Сумма розн, руб.</td>
              <td>Маржа, руб.</td>
              <td>Действие</td>
            </tr>
            <tr>
              <td>
                <Select
                  className="select"
                  options={options}
                  isSearchable="true"
                  styles={customStyles3}

                />
              </td>
              <td><input type="text" id="inputPassword6" className="form-control"></input></td>
              <td><input type="text" id="inputPassword6" className="form-control"></input></td>
              <td><input type="text" id="inputPassword6" className="form-control"></input></td>
              <td><input type="text" id="inputPassword6" className="form-control"></input></td>
              <td><input type="text" id="inputPassword6" className="form-control"></input></td>
              <td><input type="text" id="inputPassword6" className="form-control"></input></td>
              <td><input type="text" id="inputPassword6" className="form-control"></input></td>
              <td><input type="text" id="inputPassword6" className="form-control"></input></td>
              <td><input type="text" id="inputPassword6" className="form-control"></input></td>
              <td>
                <button type="button" class="btn btn-success">Сохранить</button>
              </td>
              <td>
                <button type="button" class="btn btn-danger">Отмена</button>
              </td>
            </tr>
          </table>





          <DataTable
            columns={columnsMove}
            data={dataMove}
            highlightOnHover
          />
        </Modal.Body>


        <Modal.Footer>

          <Button variant="warning" onClick={closeModal} >
            Закрыть
          </Button>
        </Modal.Footer>
      </Modal> */}

    </>

  );
};

export default Table;

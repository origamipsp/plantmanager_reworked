import React from "react";
import DataTable from "react-data-table-component";
import "./table.css";
import { useState } from "react";
import 'react-select-search/style.css'
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import SimpleMenu from '../menu/menu';





const customStyles = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,

  }),
  container: (provided, state) => ({
    ...provided,

    width: 120,
  }),
}
const customStyles3 = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,

  }),
  container: (provided, state) => ({
    ...provided,

    width: 500,
  }),
}

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];


const Table = (props) => {
  const [hideAdd, setHideAdd] = useState(true);

  // const [show, setShow] = useState(false);
  // const closeModal = () => setShow(false);
  // const showModal = (row, event) => {
  //   let myJSON = JSON.stringify(row);
  //   setSomedata(myJSON)
  //   setShow(true)
  //   console.log("row- ", row);
  //   setActiveRow(row);
  // }

  // const { setActiveRow } = props;
  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "50px",
      compact: true,
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      compact: true,

    },
    {
      name: "Со склада",
      selector: "from",
      sortable: true,
      compact: true,
    },
    {
      name: "На склад",
      selector: "to",
      sortable: true,
      compact: true,
    },
    {
      name: "Причина",
      selector: "reason",
      sortable: true,
      compact: true,
    },
    {
      name: "Исполнитель",
      selector: "executor",
      sortable: true,
      compact: true,
    },

    {
      name: "Сезон",
      selector: "season",
      sortable: true,
      compact: true,
    },

    {
      name: "Сумма закуп",
      selector: "totalpurchase",
      sortable: true,
      compact: true,
    },
    {
      name: "Сумма розница",
      selector: "totalretail",
      sortable: true,
      compact: true,
    },
    {
      name: "Сумма маржа",
      selector: "totalmargin",
      sortable: true,
      compact: true,
    },

    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,

      compact: true,

    },
  ];

  // const columnsMove = [

  //   {
  //     name: "Название",
  //     selector: "name",
  //     sortable: true,
  //     compact: true,


  //   },
  //   {
  //     name: "Сорт",
  //     selector: "type",
  //     sortable: true,
  //     compact: true,

  //   },
  //   {
  //     name: "Стандарт",
  //     selector: "standart",
  //     sortable: true,
  //     compact: true,

  //   },
  //   {
  //     name: "Сезон",
  //     selector: "season",
  //     sortable: true,
  //     compact: true,

  //   },
  //   {
  //     name: "Кол-во",
  //     selector: "count",
  //     sortable: true,
  //     compact: true,

  //   },

  //   {
  //     name: "Цена, руб.",
  //     selector: "price",
  //     sortable: true,
  //     compact: true,

  //   },

  //   {
  //     name: "Цена розн, руб.",
  //     selector: "priceRetail",
  //     sortable: true,
  //     compact: true,

  //   },
  //   {
  //     name: "Сумма, руб.",
  //     selector: "sum",
  //     sortable: true,
  //     compact: true,

  //   },
  //   {
  //     name: "Сумма розн, руб.",
  //     selector: "sumRetail",
  //     sortable: true,
  //     compact: true,

  //   },

  //   {
  //     name: "Маржа, руб.",
  //     selector: "margin",
  //     sortable: true,
  //     compact: true,

  //   },


  // ];

  return (
    <>
      <br></br>
      <p>Документов: 0</p>
      <p>Сумма закуп: 0 руб.</p>
      <p>Сумма розница: 0 руб.</p>
      <p>Маржа: 0 руб.</p>
      <hr align="center" width="100%" style={{ color: "gray" }} />
      <div className="selectWrap">
        <button type="button" class="btn btn-success" onClick={() => setHideAdd(prev => !prev)}>Добавить</button>
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'СоСклада'}
          styles={customStyles}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Исполнитель'}
          styles={customStyles}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Исполнитель'}
          styles={customStyles}
        />
        <input type="text" style={{ width: "150px" }} id="inputPassword6" className="form-control" placeholder="Коментарий"></input>
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Сезон'}
          styles={customStyles}
        />
        <div className="form-group row">
          <div className="col-xs-10">
            <input className="form-control" type="date" id="example-date-input" ></input>
          </div>
        </div>
        <div className="form-group row">
          <div className="col-xs-10">
            <input className="form-control" type="date" id="example-date-input"></input>
          </div>
        </div>
        {/* <DatePicker selected={startDate} /> */}
        <button type="button" class="btn btn-outline-success">Фильтровать</button>
        <button type="button" class="btn btn-outline-danger">Очистить</button>
        <button type="button" class="btn btn-outline-success">Отчет</button>
      </div>
      <hr align="center" width="100%" style={{ color: "gray" }} />
      <div className={`${hideAdd ? "hide-lightbox" : "lightbox"}`}>
        <div className="selectWrap">
          <table>
            <tr>
              <td>#</td>
              <td>Дата</td>
              <td>Со склада</td>
              <td>причина</td>
              <td>Исполнитель</td>
              <td colSpan={2}>Действие</td>
            </tr>
            <tr>
              <td>-14</td>
              <td>18.08.2022</td>
              <td><Select
                className="select"
                options={options}
                isSearchable="true"
                placeholder={'Со Склада'}
                styles={customStyles3}
              /></td>
              <td> <input type="text" style={{ width: "250px" }} id="inputPassword6" className="form-control" placeholder="Коментарий"></input></td>
              <td>админ</td>
              <td><button type="button" class="btn btn-success">Сохранить</button></td>
              <td><button type="button" class="btn btn-danger">Отмена</button></td>
            </tr>
          </table>
        </div>
        <hr align="center" width="100%" style={{ color: "gray" }} />

      </div>
      <DataTable
        columns={columns}
        // data={data}
        highlightOnHover
      />
    </>
  );
};

export default Table;

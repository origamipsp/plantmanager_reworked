import React from "react";
import DataTable from "react-data-table-component";
import { data } from "./data_virtual_storage";
import "./table.css";
import { useState } from "react";
import 'react-select-search/style.css'
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import TableVirtualStorageInner from "../Tables/table_virtual_storage_inner";
import SimpleMenu from '../menu/menu';




import {
  Button,
  Modal,
} from "react-bootstrap";


// const customStyles = {
//   menuList: (provided, state) => ({
//     ...provided,
//     height: 300,

//   }),
//   container: (provided, state) => ({
//     ...provided,

//     width: 120,
//   }),
// }


const customStylesSelect = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,


  }),
  container: (provided, state) => ({
    ...provided,

    width: 150,
  }),
}

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];


const Table = (props) => {

  const [show, setShow] = useState(false);
  const closeModal = () => setShow(false);
  const showModal = (row, event) => {
    let myJSON = JSON.stringify(row);
    setSomedata(myJSON)
    setShow(true)
    console.log("row- ", row);
    setActiveRow(row);
  }




  const [somedata, setSomedata] = useState(null);

  // const onRowClicked = (row, event) => {
  //   console.log("row", row);
  //   setActiveRow(row);
  // };

  const { setActiveRow } = props;
  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "20px",
      cell: (row) => <div>{row.id}</div>,
      compact: true,
    },
    {
      name: "Категория",
      selector: "category",
      sortable: true,
      compact: true,
      width: "75px",

    },
    {
      name: "Название",
      selector: "name",
      sortable: true,
      compact: true,
      width: "70px",
    },
    {
      name: "Сорт",
      selector: "type",
      sortable: true,
      width: "250px",

    },
    {
      name: "Стандарт",
      selector: "standart",
      sortable: true,
      compact: true,
    },
    {
      name: "Ед. изм.",
      selector: "unit",
      sortable: true,
      compact: true,
    },
    {
      name: "Сезон",
      selector: "season",
      sortable: true,
      compact: true,
    },
    {
      name: "Группа доставки",
      selector: "deliveryGroup",
      sortable: true,
      compact: true,
    },
    {
      name: "Поставщик",
      selector: "supplier",
      sortable: true,
      compact: true,
    },
    {
      name: "Закуп, руб.",
      selector: "purchase",
      sortable: true,
      compact: true,
    },
    {
      name: "Продажа, руб.",
      selector: "sell",
      sortable: true,
      compact: true,
    },
    {
      name: "Приход",
      selector: "income",
      sortable: true,
      compact: true,
    },
    {
      name: "Расход",
      selector: "consumption",
      sortable: true,
      compact: true,
    },
    {
      name: "Остаток",
      selector: "remainder",
      sortable: true,
      compact: true,
    },
    {
      name: "Факт приход",
      selector: "factIncome",
      sortable: true,
      compact: true,
    },
    {
      name: "Факт цена",
      selector: "factPrice",
      sortable: true,
      compact: true,
    },
    {
      name: "Статус",
      selector: "status",
      sortable: true,
      compact: true,
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,

      compact: true,
    },
  ];

  return (
    <>

      <div className="selectWrap">
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'категория'}
          styles={customStylesSelect}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Название'}
          styles={customStylesSelect}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Стандарт'}
          styles={customStylesSelect}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Сезон'}
          styles={customStylesSelect}
        />

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Доставка'}
          styles={customStylesSelect}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Поставщик'}
          styles={customStylesSelect}
        />

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Статус'}
          styles={customStylesSelect}
        />




        <div className="col-xs-10">
          <input className="form-control" type="date" id="example-date-input"></input>
        </div>





        <div className="col-xs-10">
          <input className="form-control" type="date" id="example-date-input"></input>
        </div>

        {/* <DatePicker selected={startDate} /> */}

        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Остатки'}
          styles={customStylesSelect}
        />
        <button type="button" class="btn btn-outline-success">Фильтровать</button>
        <button type="button" class="btn btn-outline-danger">Очистить</button>
        <button type="button" class="btn btn-outline-success">Excel</button>


      </div>

      <div className="tabWrap">
        <DataTable
          pointerOnHover={true}
          onRowClicked={showModal}
          columns={columns}
          data={data}
          highlightOnHover
        />
      </div>

      <Modal size="xl" scrollable 
        aria-labelledby="contained-modal-title-vcenter"
        centered show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Дополнительная информация</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody" style={{ height: 700}}>
          <TableVirtualStorageInner setActiveRow={setActiveRow} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={closeModal} >
            Закрыть
          </Button>
        </Modal.Footer>
      </Modal>


    </>

  );
};

export default Table;

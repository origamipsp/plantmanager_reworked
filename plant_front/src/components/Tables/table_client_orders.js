import React from "react";
import DataTable from "react-data-table-component";
import { data } from "./data_import_client_order";
import { dataGoods } from "./data_goods";
import { dataDelivery } from "./data_delivery";
import { dataPayments } from "./data_payments";
import { dataPayments2 } from "./data_payments2";
import "./table.css";
import { useState } from "react";
import SimpleMenu from '../menu/menu';
import Select from 'react-select';
import { Line } from '../../components/Line/line'
import InputGroup from 'react-bootstrap/InputGroup';
import {
  Button,
  Form,
  Modal,
} from "react-bootstrap";
const FilterComponent = ({ filterText, onFilter, onClear }) => (
  <>
    <input
      id="search"
      type="text"
      placeholder="Фильтр по наименованию"
      aria-label="Search Input"
      value={filterText}
      onChange={onFilter}
    ></input>
    <Button type="button" onClick={onClear}>
      X
    </Button>
  </>
);


// const CustomMenu = React.forwardRef(
//   ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
//     const [value, setValue] = useState('');

//     return (
//       <div
//         ref={ref}
//         style={style}
//         className={className}
//         aria-labelledby={labeledBy}
//       >
//         <Form.Control
//           autoFocus
//           className="mx-3 my-2 w-auto"
//           placeholder="Type to filter..."
//           onChange={(e) => setValue(e.target.value)}
//           value={value}
//         />
//         <ul className="list-unstyled">
//           {React.Children.toArray(children).filter(
//             (child) =>
//               !value || child.props.children.toLowerCase().startsWith(value),
//           )}
//         </ul>
//       </div>
//     );
//   },
// );


const customStylesSelect = {
  menuList: (provided, state) => ({
    ...provided,
    height: 300,

  }),
  container: (provided, state) => ({
    ...provided,
    width: 250,
  }),
}


const Table = (props) => {

  const [show, setShow] = useState(false);
  const closeModal = () => setShow(false);
  const showModal = (row, event) => {

    setRow(row)
    setShow(true)
    console.log("row- ", row);
    console.log("row-!! ", row.id);
    setActiveRow(row);

  }
  const [row, setRow] = useState(null);



  const { setActiveRow } = props;
  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "60px",
      compact: true,
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      width: "100px",
      compact: true,
    },
    {
      name: "Клиент",
      selector: "client",
      sortable: true,
      width: "300px",
      compact: true,
    },
    {
      name: "Ник",
      selector: "nickName",
      sortable: true,
      width: "150px",
      compact: true,
    },
    {
      name: "Происхождение",
      selector: "origin",
      sortable: true,
      compact: true,
    },
    {
      name: "Сезон",
      selector: "season",
      sortable: true,
      compact: true,
    },
    {
      name: "Сумма",
      selector: "sum",
      sortable: true,
      compact: true,
    },
    {
      name: "Оплачено",
      selector: "payment",
      sortable: true,
      compact: true,
    },
    {
      name: "Тип оплаты",
      selector: "paymentType",
      sortable: true,
      compact: true,
    },
    {
      name: "Доставка",
      selector: "delivery",
      sortable: true,
      compact: true,
    },
    {
      name: "Статус",
      selector: "status",
      sortable: true,
      compact: true,
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} menuItems={[{ title: ["Изменить", "Отчет", "Доставка"], onClick: () => { console.log("123qwe") } }]} />,
      allowOverflow: true,
      button: true,
      compact: true,
    },
  ];

  const columnsDelivery = [
    {
      name: "Название",
      selector: "name",
      sortable: true,
      compact: true,
    },
    {
      name: "Сорт",
      selector: "type",
      sortable: true,
      compact: true,
    },
    {
      name: "Стандарт",
      selector: "standart",
      sortable: true,
      width: "100px",
      compact: true,
    },
    {
      name: "Сезон",
      selector: "season",
      sortable: true,
      width: "80px",
      compact: true,
    },
    {
      name: "Кол-во",
      selector: "count",
      sortable: true,
      width: "80px",
      compact: true,
    },
    {
      name: "Ед. изм.",
      selector: "unit",
      sortable: true,
      width: "100px",
      compact: true,
    },
    {
      name: "Цена, руб.",
      selector: "price",
      sortable: true,
      width: "100px",
      compact: true,
    },
    {
      name: "Сумма, руб",
      selector: "sum",
      sortable: true,
      width: "100px",
      compact: true,
    },
    {
      name: "Статус",
      selector: "status",
      sortable: true,
      compact: true,
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} menuItems={[{ title: ["Изменить", "Отчет", "Доставка"], onClick: () => { console.log("123qwe") } }]} />,
      allowOverflow: true,
      button: true,
      compact: true,
    },
  ];


  const columnsPayments = [
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      compact: true,
    },
    {
      name: "Назначение",
      selector: "purpose",
      sortable: true,
      compact: true,
    },
    {
      name: "	Сумма",
      selector: "sum",
      sortable: true,
      compact: true,
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,
      compact: true,
    },
  ];

  const columnsPayments2 = [

    {
      name: "Счёт №",
      selector: "bill",
      sortable: true,
      compact: true,
    },
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      compact: true,
    },
    {
      name: "Сумма",
      selector: "sum",
      sortable: true,
      compact: true,
    },
    {
      name: "Назначение",
      selector: "purpose",
      sortable: true,
      compact: true,
    },
    {
      name: "Статус",
      selector: "status",
      sortable: true,
      compact: true,
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,
      compact: true,
    },
  ];


  // const seasons = {
  //   1: "весна",
  //   2: "лето",
  //   3: "осень",
  //   4: "зима",
  //   5: "всегда"
  // };

  // const suppliers = {
  //   1: "ООО Успех",
  //   2: "Мистер Лого",
  //   3: "Собственное производство",
  // };

  // const statuses = {
  //   1: "черновик",
  //   2: "в работе",
  //   3: "завершён",
  // };




  const [filterText, setFilterText] = React.useState('');
  const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
  const filteredItems = dataGoods.filter(
    item => item.name && item.name.toLowerCase().includes(filterText.toLowerCase()),
  );

  const subHeaderComponentMemo = React.useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText('');
      }
    };

    return (
      <FilterComponent onFilter={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} />
    );
  }, [filterText, resetPaginationToggle]);

  const [hideParcels, setHideParcels] = useState(true);
  const [hidePayments, setHidePayments] = useState(true);
  // const [hideAddPayments, setHideAddPayments] = useState(true);

  // const [season, setSeason] = useState("");
  // const [supplier, setSupplier] = useState("");
  // const [status, setStatus] = useState("");

  const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
  ];


  return (
    <>

      <div className="selectWrap">
        <button type="button" class="btn btn-success">Добавить</button>
        <input type="text" style={{ width: "150px" }} id="inputPassword6" className="form-control" aria-describedby="passwordHelpInline" placeholder="Категория"></input>
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Происхождение'}
          styles={customStylesSelect}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Статус'}
          styles={customStylesSelect}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Сезон'}
          styles={customStylesSelect}
        />
        <Select
          className="select"
          options={options}
          isSearchable="true"
          placeholder={'Оплаты'}
          styles={customStylesSelect}
        />

        <div className="col-xs-10">
          <input className="form-control" type="date" id="example-date-input"></input>
        </div>

        <div className="col-xs-10">
          <input className="form-control" type="date" id="example-date-input"></input>
        </div>

        <button type="button" class="btn btn-outline-success">Фильтровать</button>
        <button type="button" class="btn btn-outline-danger">Очистить</button>
      </div>


      <DataTable
        pointerOnHover={true}
        onRowClicked={showModal}
        columns={columns}
        data={data}
        highlightOnHover
      />

      <Modal size="xl" show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Дополнительно</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>



          <div className="container">
            <div className="row ">
              <div className="col text-left">
                <p>Адрес доставки: 656054, Алтайский край, г.Барнаул, ул. А.Петрова, д.190, кв.57</p>
                <p>Комментарий покупателя:</p>
                <p>Комментарий к заказу:</p>
                <p>Скидка: 0%</p>
              </div>
              <div className="col">
                <div className="row">
                  <div className="col">
                    <span><button type="button" className="btn btn-success btn-sm" onClick={() => setHidePayments(prev => !prev)} >Оплаты</button></span>
                  </div>

                  <div className="col">
                    <span><button type="button" className="btn btn-success btn-sm" onClick={() => setHideParcels(prev => !prev)}>Посылки</button></span>
                  </div>

                </div>
              </div>
              <div className="col">
                <p>Последнее изменение заказа: 22.08.2022</p>
              </div>
            </div>
          </div>

          <Line />
          <hr align="center" width="100%" style={{ color: "gray" }} />

          <p>Товаров в заказе: 2, На сумму: 525 руб.</p>

          <InputGroup size="sm" className="mb-3" style={{ width: "150px", height: "20px" }}>
            <InputGroup.Text id="basic-addon3">Скидка, %</InputGroup.Text>
            <Form.Control
              value="0"
              aria-label="Username"
              aria-describedby="basic-addon3"
            />
          </InputGroup>
          <div className="wrapInline">
            <button type="button" className="btn btn-sm btn-success" >Добавить товар</button>
            <input type="text" style={{ width: "150px" }} id="inputPassword6" className="form-control" aria-describedby="passwordHelpInline" placeholder="Наименование"></input>
            <button type="button" className="btn btn-sm btn-outline-success" >Фильтровать</button>
            <button type="button" className="btn btn-sm btn-outline-danger" >Очистить</button>
          </div>

          <div className="d-flex justify-content-end">
            <h2>Осень</h2>
          </div>
          <ul>
            <li>Срок поставки: Группа №3</li>
          </ul>

          <Select
            className="select"
            options={options}
            isSearchable="true"
            placeholder={'Доставка'}
            styles={customStylesSelect}
          />
          <hr align="center" width="100%" style={{ color: "gray" }} />

          <div className={`${hidePayments ? "hide-lightbox" : "lightbox"}`}>
            <p>Оплат: 0 На сумму: 0</p>
            <div class="container">
              <div class="row">
                <div class="col-sm">

                  <DataTable
                    onRowClicked={showModal}
                    columns={columnsPayments}
                    data={dataPayments}
                    highlightOnHover
                  />
                </div>
                <div class="col-sm">
                  <DataTable
                    columns={columnsPayments2}
                    data={dataPayments2}
                    highlightOnHover
                  />
                </div>
              </div>
            </div>
            <hr align="center" width="100%" style={{ color: "gray" }} />
          </div>

          <DataTable
            columns={columnsDelivery}
            data={dataDelivery}
            highlightOnHover
          />

          <div className={`${hideParcels ? "hide-lightbox" : "lightbox"}`} >
            <h3>Посылки сборка</h3>
            <div className="wrapInline">
              <input type="text" style={{ width: "150px" }} id="inputPassword6" className="form-control" aria-describedby="passwordHelpInline" placeholder="Наименование"></input>
              <button type="button" className="btn btn-outline-success" >Фильтровать</button>
              <button type="button" className="btn btn-outline-danger">Очистить</button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Table;

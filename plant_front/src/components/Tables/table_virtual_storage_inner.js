import React from "react";
import DataTable from "react-data-table-component";
import { data } from "./data_virtual_storage_inner";
import "./table.css";
import { useState } from "react";
import 'react-select-search/style.css'
import "react-datepicker/dist/react-datepicker.css";
import TableVirtualStorageInnerInner from "../Tables/table_virtual_storage_inner_inner";
import SimpleMenu from '../menu/menu';
import { Line } from '../../components/Line/line'
import InputGroup from 'react-bootstrap/InputGroup';




import {
  Button,
  Form,
  Modal,
} from "react-bootstrap";


const Table = (props) => {

  const [show, setShow] = useState(false);
  const closeModal = () => setShow(false);
  const showModal = (row, event) => {
    let myJSON = JSON.stringify(row);
    setSomedata(myJSON)
    setShow(true)
    console.log("row- ", row);
    setActiveRow(row);
  }


  // const [showAdd, setShowAdd] = useState(false);
  // const closeModalAdd = () => setShowAdd(false);
  // const showModalAdd = () => {
  //   setShowAdd(true)
  // }

  const [somedata, setSomedata] = useState(null);

  const [hidePayments, setHidePayments] = useState(true);
  const [hidePackages, setHidePackages] = useState(true);


  // const onRowClicked = (row, event) => {
  //   console.log("row", row);
  //   setActiveRow(row);
  // };




  const ExpandedComponent = ({ data }) => <pre>

    {/* {JSON.stringify(data, null, 2)} */}


    <div className="container text-center">
      <div className="row">
        <div className="col" style={{ textAlign: "left" }}>
          <p>Адрес доставки: 442680 Пензенская обл,<br></br>г.Никольск, ул.Тургенева д.3</p>
          <p>Комментарий покупателя: Чат</p>
          <p>Комментарий к заказу:</p>
          <p>Скидка: 0%</p>
        </div>
        <div className="col">
          <button type="button" class="btn btn-success btn-sm" onClick={() => setHidePayments(prev => !prev)}>Оплаты</button>
        </div>
        <div className="col">
          <button type="button" class="btn btn-success btn-sm" onClick={() => setHidePackages(prev => !prev)}>Посылки</button>
        </div>


        <div className="col">
          <p>Последнее изменение заказа: 29.07.2022</p>
        </div>
      </div>
    </div>




    <Line />
    <hr align="center" width="100%" style={{ color: "gray" }} />


    <div className={`${hidePayments ? "hide-lightbox" : "lightbox"}`}>
      <p>Оплат: 0 На сумму: 0</p>
      <div class="container">
        <div class="row">
          <div class="col-sm">

            <DataTable
              pointerOnHover={true}
              onRowClicked={showModal}
              columns={paymentColumns}
              data={paymentData}
              highlightOnHover

            />
          </div> 
          <div class="col-sm">
            <DataTable
              pointerOnHover={true}
              // onRowClicked={showModal}
              columns={checkColumns}
              data={checkData}
              highlightOnHover
            // expandableRows
            // expandableRowsComponent={ExpandedComponent}
            />
          </div>

        </div>
      </div>
      <hr align="center" width="100%" style={{ color: "gray" }} />
    </div>
    


    <p>Товаров в заказе: 6, На сумму: 3160 руб.</p>

    <InputGroup size="sm" className="mb-3" style={{ width: "150px" }}>
      <InputGroup.Text id="basic-addon3">Скидка, %</InputGroup.Text>
      <Form.Control
        value="0"
        aria-label="Username"
        aria-describedby="basic-addon3"
      />
    </InputGroup>
    <button type="button" class="btn btn-success">Добавить товар</button>



    <div className="d-flex justify-content-end">
      <h2>Осень</h2>
    </div>

    <ul>
      <li>Срок поставки: в наличии</li>
    </ul>
    <hr align="center" width="100%" style={{ color: "gray" }} />

    <DataTable
      pointerOnHover={true}
      // onRowClicked={showModal}
      // columns={paymentColumns}
      //  data={paymentData}
      highlightOnHover
      expandableRows
      expandableRowsComponent={ExpandedComponent}
    />

    <ul>
      <li>Срок поставки: менее 10 дней</li>
    </ul>
    <hr align="center" width="100%" style={{ color: "gray" }} />
    <DataTable
      pointerOnHover={true}
      // onRowClicked={showModal}
      // columns={paymentColumns}
      // data={paymentData}
      highlightOnHover
      expandableRows
      expandableRowsComponent={ExpandedComponent}
    />

    {/* <div className="d-flex justify-content-end"> */}



    {/* <DataTable
  pointerOnHover={true}
  onRowClicked={showModal}
  columns={columns}
  data={data}
  highlightOnHover
/> */}

    <div className={`${hidePackages ? "hide-lightbox" : "lightbox"}`}>
      <hr align="center" width="100%" style={{ color: "gray" }} />
      <h2>Посылки сборка</h2>


      <div class="container" style={{ width: 500 }}>
        <div class="row">
          <div class="col-sm">
            <input type="email" style={{ width: 200 }} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Наименование, сорт"></input>
          </div>
          <div class="col-sm">
            <button type="button" className="btn btn-outline-success ">Фильтровать</button>
          </div>
          <div class="col-sm">
            <button type="button" className="btn btn-outline-danger ">Очистить</button>
          </div>
        </div>
      </div>



    </div>


    {/* </div> */}
  </pre>;






  const { setActiveRow } = props;
  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "40px",
      compact: true,
      cell: (row) => <div>{row.id}</div>
    },

    {
      name: "Дата",
      selector: "date",
      sortable: true,
      width: "75px",
      compact: true,

    },
    {
      name: "Клиент",
      selector: "client",
      sortable: true,
      width: "205px",
      compact: true,
    },
    {
      name: "Ник",
      selector: "nickName",
      sortable: true,
      width: "205px",
      compact: true,
    },
    {
      name: "Происхождение",
      selector: "origin",
      sortable: true,
      width: "110px",
      compact: true,
    },
    {
      name: "Сезон",
      selector: "season",
      sortable: true,
      width: "60px",
      compact: true,
    },
    {
      name: "Сумма",
      selector: "sum",
      sortable: true,
      width: "55px",
      compact: true,

    },
    {
      name: "Оплачено",
      selector: "paid",
      sortable: true,
      width: "75px",
      compact: true,
    },
    {
      name: "Тип оплаты",
      selector: "paymentType",
      sortable: true,
      width: "75px",
      compact: true,
    },
    {
      name: "Статус",
      selector: "status",
      sortable: true,
      width: "55px",
      compact: true,
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,
      compact: true,
      width: "80px",
    },

  ];


  const checkColumns = [
    {
      name: "Счёт №",
      selector: "checkNum",
      sortable: true,
      width: "90px",
      compact: true,
      // cell: (row) => <div>{row.id}</div>
    },

    {
      name: "Дата",
      selector: "date",
      sortable: true,
      width: "100px",
      compact: true,

    },
    {
      name: "Сумма",
      selector: "sum",
      sortable: true,
      width: "85px",
      compact: true,

    },
    {
      name: "Назначение",
      selector: "purpose",
      sortable: true,
      width: "100px",
      compact: true,

    },
    {
      name: "Статус",
      selector: "status",
      sortable: true,
      width: "85px",
      compact: true,

    },

    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,
      compact: true,
      width: "55px",
    },

  ];


  const checkData = [
    {
      id: "1",
      checkNum: "02253-6787",
      date: "16.08.2022",
      sum: "3160",
      purpose: "оплата",
      status: "выставлен",
    },

  ]


  const paymentColumns = [
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      width: "140px",
      compact: true,

    },
    {
      name: "Назначение",
      selector: "purpose",
      sortable: true,
      width: "140px",
      compact: true,

    },

    {
      name: "Сумма",
      selector: "sum",
      sortable: true,
      width: "140px",
      compact: true,

    },

    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,
      compact: true,
      width: "100px",
    },

  ];


  const paymentData = [
    {
      id: "1",
      checkNum: "02253-6787",
      date: "16.08.2022",
      sum: "3160",
      purpose: "оплата",
      status: "выставлен",
    },

  ]

  return (
    <>
      <DataTable
        pointerOnHover={true}
        onRowClicked={showModal}
        columns={columns}
        data={data}
        highlightOnHover
        expandableRows
        expandableRowsComponent={ExpandedComponent}
      />






      <Modal size="xl" scrollable
        aria-labelledby="contained-modal-title-vcenter"
        centered show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Дополнительная информация о заказе</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody">
          <TableVirtualStorageInnerInner setActiveRow={setActiveRow} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={closeModal} >
            Закрыть
          </Button>
        </Modal.Footer>
      </Modal>





    </>

  );
};

export default Table;

import React from "react";
import DataTable from "react-data-table-component";
import { data } from "./data_virtual_storage_inner_inner";
import "./table.css";
import { useState } from "react";
import 'react-select-search/style.css'
import "react-datepicker/dist/react-datepicker.css";
import TableVirtualStorageInnerInner from "../Tables/table_virtual_storage_inner_inner";
import { Line } from '../../components/Line/line'



import {
  Button,
  Modal,
} from "react-bootstrap";


const Table = (props) => {

  const [show, setShow] = useState(false);
  const closeModal = () => setShow(false);
  const showModal = (row, event) => {
    let myJSON = JSON.stringify(row);
    setSomedata(myJSON)
    setShow(true)
    console.log("row- ", row);
    setActiveRow(row);
  }


  // const [showAdd, setShowAdd] = useState(false);
  // const closeModalAdd = () => setShowAdd(false);
  // const showModalAdd = () => {
  //   setShowAdd(true)
  // }

  const [somedata, setSomedata] = useState(null);

  // const onRowClicked = (row, event) => {
  //   console.log("row", row);
  //   setActiveRow(row);
  // };

  const { setActiveRow } = props;
  const columns = [
    {
      name: "Название",
      selector: "name",
      sortable: true,

    },
    {
      name: "Сорт",
      selector: "type",
      sortable: true
    },
    {
      name: "Стандарт",
      selector: "standart",
      sortable: true
    },
    {
      name: "Сезон",
      selector: "season",
      sortable: true
    },
    {
      name: "	Кол-во",
      selector: "count",
      sortable: true
    },
    {
      name: "Ед. изм.",
      selector: "unit",
      sortable: true
    },
    {
      name: "Цена, руб.",
      selector: "price",
      sortable: true
    },
    {
      name: "	Сумма, руб.",
      selector: "sum",
      sortable: true
    },
    {
      name: "Статус",
      selector: "status",
      sortable: true
    },

  ];

  return (
    <>
      <p>Последнее изменение заказа: 29.07.2022</p>
      <p>Адрес доставки: 442680 Пензенская обл, г.Никольск, ул.Тургенева д.3</p>
      <p>Комментарий покупателя: Чат</p>
      <p>Комментарий к заказу:</p>
      <p>Скидка: 0%</p>
  
      <button type="button" class="btn btn-success">Оплаты</button>
      <button type="button" class="btn btn-success">Посылки</button>
      <Line />

      <DataTable
        pointerOnHover={true}
        onRowClicked={showModal}
        columns={columns}
        data={data}
        highlightOnHover
      />

      <Modal size="xl" scrollable
        aria-labelledby="contained-modal-title-vcenter"
        centered show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Дополнительная информация</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody">
          <TableVirtualStorageInnerInner setActiveRow={setActiveRow} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={closeModal} >
            Закрыть
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Table;

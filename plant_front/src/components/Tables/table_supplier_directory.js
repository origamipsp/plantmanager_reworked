import React from "react";
import DataTable from "react-data-table-component";
import { data } from "./data_supplier_directory";
import "./table.css";
import { useState } from "react";
import {
  Button,
  Modal,
  InputGroup,
  Form,
} from "react-bootstrap";


const Table = (props) => {

  const [showEdit, setShowEdit] = useState(false);
  const closeModalEdit = () => setShowEdit(false);
  const showModalEdit = (row, event) => {
    let myJSON = JSON.stringify(row);
    setSomedata(myJSON)
    setShowEdit(true)
    console.log("row- ", row);
    setActiveRow(row);

  }

  
  const [somedata, setSomedata] = useState(null);

  // const onRowClicked = (row, event) => {
  //   console.log("row", row);
  //   setActiveRow(row);
  // };

  const { setActiveRow } = props;
  const columns = [
    {
      name: "ID",
      selector: "id",
      sortable: true,
      width: "10px",
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Компания",
      selector: "companyname",
      sortable: true,

    },
    {
      name: "Телефон",
      selector: "phone",
      sortable: true
    },
    {
      name: "Email",
      selector: "email",
      sortable: true
    },
    {
      name: "Комментарий",
      selector: "comment",
      sortable: true
    },
    {
      name: "Адрес",
      selector: "address",
      sortable: true
    },
  ];



  return (
    <>
      <DataTable
        onRowClicked={showModalEdit}
        columns={columns}
        data={data}
        highlightOnHover
      />

      <Modal centered size="xl" show={showEdit} onHide={closeModalEdit}>
        <Modal.Header>
          <Modal.Title>Изменить "Поставщика"</Modal.Title>
          <Button variant="light" onClick={closeModalEdit}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>


          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Компания</InputGroup.Text>
                  <Form.Control
                    // onChange={changeHandler}
                    name="company"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Телефон</InputGroup.Text>
                  <Form.Control
                    // onChange={changeHandler}
                    name="company"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Email</InputGroup.Text>
                  <Form.Control
                    // onChange={changeHandler}
                    name="company"
                  />
                </InputGroup>
              </div>
            </div>

          </div>


          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Адрес</InputGroup.Text>
                  <Form.Control
                    // onChange={changeHandler}
                    name="company"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Комментарий</InputGroup.Text>
                  <Form.Control
                    // onChange={changeHandler}
                    name="company"
                  />
                </InputGroup>
              </div>

            </div>

          </div>







        </Modal.Body>
        <Modal.Footer>
          <Button variant="success">
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>






    </>

  );
};

export default Table;

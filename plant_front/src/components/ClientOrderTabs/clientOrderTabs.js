import './clientOrderTabs.css';
import { Tabs, Tab } from "react-bootstrap";
import { useState } from 'react';
import TableClientOrder from "../Tables/table_client_orders";


export const ClientOrderTabs = ({ className = '', visible = true }) => {
    const [key, setKey] = useState("virtual");
    const [activeRow, setActiveRow] = useState({});
    return (
        <div className='wrap'>
            <Tabs
                className="tabs"
                id="controlled-tab-example"
                activeKey={key}
                onSelect={(k) => setKey(k)}
            >
                <Tab eventKey="virtual" title="Заказы клиентов">
                    <p>Заказы по статусам: готов: 9, доставка: 1, завершён: 1785, непоставка: 1, ожидаем: 98, отменён: 308</p>
                    <p>Всего заказов: 1894, на сумму: 5 929 131,85, оплат на сумму: 6 330 834, доставка на сумму: 414 998,2</p>
                    <div>
                        <TableClientOrder setActiveRow={setActiveRow} />
                    </div>
                </Tab>
                <Tab eventKey="real" title="Сборка">
                    <div>
                        {/* <TableReal setActiveRow={setActiveRow} /> */}
                    </div>
                </Tab>

                <Tab eventKey="moving" title="Отправка">
                {/* <TableStorageMove setActiveRow={setActiveRow} /> */}
                </Tab>
                
            </Tabs>
        </div>
    );
}
export const units = [
    {
        id: "1",
        unit: "шт.",
    },
    {
        id: "2",
        unit: "кг.",
    },
    {
        id: "3",
        unit: "бут.",
    },
    {
        id: "4",
        unit: "л.",
    },
    {
        id: "5",
        unit: "уп.",
    },
    {
        id: "6",
        unit: "пачка",
    },
];

export const deliveryTime = [
    {
        id: "1",
        value: "Группа №1",
    },
    {
        id: "2",
        value: "Группа №2",
    },
    {
        id: "3",
        value: "Группа №3",
    },
    {
        id: "4",
        value: "в наличии",
    },
];

export const seasonality = [
    {
        id: "1",
        season: "всегда",
    },
    {
        id: "2",
        season: "весна",
    },
    {
        id: "3",
        season: "осень",
    },
];


export const cuctomerSource = [
    {
        id: "1",
        value: "другое",
    },
    {
        id: "2",
        value: "одноклассники",
    },
    {
        id: "3",
        value: "вконтакте",
    },
    {
        id: "4",
        value: "инстаграм",
    },
    {
        id: "5",
        value: "сайт",
    },
    {
        id: "6",
        value: "сми",
    },
    {
        id: "7",
        value: "листовки",
    },
    {
        id: "8",
        value: "наружняя реклама",
    },
    {
        id: "9",
        value: "сарафанное радио",
    },
];

export const storagePeriods = [
    {
        id: "1",
        value: "до месяца",
    },
    {
        id: "2",
        value: "до 2х недель",
    },
    {
        id: "3",
        value: "требует спец. условий",
    },
    {
        id: "4",
        value: "не рекомендуется",
    },
];


export const paymentType = [
    {
        id: "1",
        value: "нал.",
    },
    {
        id: "2",
        value: "б/нал.",
    },
];

export const paymentPurpose = [
    {
        id: "1",
        value: "предоплата",
    },
    {
        id: "2",
        value: "оплата",
    },
    {
        id: "3",
        value: "оплата доставки",
    },
];

export const customerOrderStatus = [
    {
        id: "1",
        value: "черновик",
    },
    {
        id: "2",
        value: "ожидаем",
    },
    {
        id: "3",
        value: "сборка",
    },
    {
        id: "4",
        value: "готов",
    },
    {
        id: "5",
        value: "доставка",
    },
    {
        id: "6",
        value: "завершён",
    },
    {
        id: "7",
        value: "отменён",
    },
    {
        id: "8",
        value: "непоставка",
    },
];


export const supplierOrderStatus = [
    {
        id: "1",
        value: "черновик",
    },
    {
        id: "2",
        value: "в работе",
    },
    {
        id: "3",
        value: "завершён",
    },
    {
        id: "4",
        value: "отказ",
    },
];

export const itemStatus = [
    {
        id: "1",
        value: "черновик",
    },
    {
        id: "2",
        value: "ожидаем",
    },
    {
        id: "3",
        value: "получен",
    },
    {
        id: "4",
        value: "отказ",
    },
];

export const storages = [
    {
        id: "1",
        value: "виртуальный",
    },
    {
        id: "2",
        value: "розница",
    },
    {
        id: "3",
        value: "предзаказ",
    },
    {
        id: "4",
        value: "списано",
    },
];

export const writeOffReasons = [
    {
        id: "1",
        value: "	продажа",
    },
    {
        id: "2",
        value: "	перемещение",
    },
    {
        id: "3",
        value: "приход",
    },
    {
        id: "4",
        value: "порча",
    },
    {
        id: "5",
        value: "утрата",
    },
];

export const productCategories = [
    {
        id: "1",
    },

];
//
export const users = [
    {
        id: "1",
        name: "admin",
        Email: "admin@admin.ru",
        role: "Админ",
        condition: "Активен",
    },
];
  //




export const units = {
    1: "шт.",
    2: "кг.",
    3: "бут.",
    4: "л.",
    5: "уп.",
    6: "пачка",
};





export const deliveryTime =
{
    1: "Группа №1",
    2: "Группа №2",
    3: "Группа №3",
    4: "в наличии",
}



export const seasonality = {
    1: "всегда",
    2: "весна",
    3: "осень",
};

export const sources =
{
    1: "другое",
    2: "одноклассники",
    3: "вконтакте",
    4: "инстаграм",
    5: "сайт",
    6: "сми",
    7: "листовки",
    8: "наружняя реклама",
    9: "сарафанное радио",
}


export const storagePeriods = {
    1: "до месяца",
    2: "до 2х недель",
    3: "требует спец. условий",
    4: "не рекомендуется",
}



export const paymentType = [
    {
        id: "1",
        value: "нал.",
    },
    {
        id: "2",
        value: "б/нал.",
    },
];

export const paymentPurpose = [
    {
        id: "1",
        value: "предоплата",
    },
    {
        id: "2",
        value: "оплата",
    },
    {
        id: "3",
        value: "оплата доставки",
    },
];

export const customerOrderStatus = {
    1: "черновик",
    2: "ожидаем",
    3: "сборка",
    4: "готов",
    5: "доставка",
    6: "завершён",
    7: "отменён",
    8: "непоставка",
    8: "в работе",
}



export const supplierOrderStatus = [
    {
        id: "1",
        value: "черновик",
    },
    {
        id: "2",
        value: "в работе",
    },
    {
        id: "3",
        value: "завершён",
    },
    {
        id: "4",
        value: "отказ",
    },
];

export const itemStatus = {
    1: "черновик",
    2: "ожидаем",
    3: "получен",
    4: "отказ",
}

export const storages = [
    {
        id: "1",
        value: "виртуальный",
    },
    {
        id: "2",
        value: "розница",
    },
    {
        id: "3",
        value: "предзаказ",
    },
    {
        id: "4",
        value: "списано",
    },
];

export const writeOffReasons = [
    {
        id: "1",
        value: "	продажа",
    },
    {
        id: "2",
        value: "	перемещение",
    },
    {
        id: "3",
        value: "приход",
    },
    {
        id: "4",
        value: "порча",
    },
    {
        id: "5",
        value: "утрата",
    },
];

export const productCategories = [
    {
        id: "1",
    },

];
//
export const users = [
    {
        id: "1",
        name: "admin",
        Email: "admin@admin.ru",
        role: "Админ",
        condition: "Активен",
    },
];
  //

import { Layout } from '../components/Layout/Layout'
import { Line } from '../components/Line/line'
import React, { useEffect } from 'react'
import { useHttp } from "../hooks/http.hook"
import { useState } from 'react';
import {
  Button,
  Modal,
  InputGroup,
  Form,
} from "react-bootstrap";
import SimpleMenu from '../components/menu/menu';
import DataTable from "react-data-table-component";
import Select from 'react-select';
import { seasonality } from '../data/select_config_data'
import { units } from '../data/select_config_data'
import { storagePeriods } from '../data/select_config_data'




export const GoodsDirectory = (props) => {

  const { request } = useHttp()

  const [form, setForm] = useState({
    category: "",
    name: "",
    type: "",
    unit: "",
    season: "",
    standart: "",
    lastsupplier: "",
    lastpraice: "",
    markup: "",
    manufacturer: "",
    barcode: "",
    weight: "",
    length: "",
    width: "",
    height: "",
    description: "",
    storing: "",

  })

  const [goods, setGoods] = useState(null)
  const [goodsCategory, setGoodsCategory] = useState(null)
  const [currentItem, setCurrentItem] = useState(null)
  const [showAdd, setShowAdd] = useState(false);
  const closeModalAdd = () => setShowAdd(false);
  const showModalAdd = (row, event) => {
    setShowAdd(true)
  }

  const [showEdit, setShowEdit] = useState(false);
  const closeModalEdit = () => setShowEdit(false);

  const showModalEdit = (row, event) => {
    setForm(
      {
        category: row.category,
        name: row.name,
        type: row.type,
        unit: row.unit,
        season: row.season,
        standart: row.standart,
        lastsupplier: row.lastsupplier,
        lastpraice: row.lastpraice,
        markup: row.markup,
        manufacturer: row.manufacturer,
        barcode: row.barcode,

        weight: row.weight,
        length: row.length,
        width: row.width,
        height: row.height,
        description: row.description,
        storing: row.storing,
      }
    )
    setShowEdit(true)
    setCurrentItem(row.id)
  }


  useEffect(() => {
    (async () => {
      await getAllGoods()
      await getAllGoodsCategory()
      // console.log("useEffect-getAllGoods--", goods)
      // console.log("useEffect-getAllGoodsCategory--", goodsCategory)
    })()
  }, [])


  const changeHandler = e => {
    setForm(prev => ({ ...prev, [e.target.name]: e.target.value }))
  }

  const [selectedCategory, setSelectedCategory] = useState(null);
  const handleSelectChange = (values) => {
    setSelectedCategory(values);
    setForm(prev => ({ ...prev, category: values.value }))
  };

  const customStylesSelect = {
    menuList: (provided, state) => ({
      ...provided,
      height: 300,
    }),
    container: (provided, state) => ({
      ...provided,
    }),
  }
  
  async function getAllGoods() {
    try {
      const data = await request('/api/getAllGoods', 'GET')
      console.log("getAllGoods-", data)
      setGoods(data.goods)
    } catch (error) {
      setGoods([])
    }
  }

  async function getAllGoodsCategory() {
    console.log("getAllGoodsCategory?")
    try {
      const data = await request('/api/getAllGoodsCategory', 'GET')
      console.log("getAllGoodsCategory-", data)
      setGoodsCategory(data.goodsCategoryResultArr)
    } catch (error) {
      setGoodsCategory([])
    }
  }

  async function addClick() {
    try {
      setGoods(await request('/api/addGoods', 'POST', { ...form }))
    } catch (error) {
      console.log(error)
    }
    closeModalAdd()
  }

  async function saveClick() {
    console.log("form-", { ...form, id: currentItem })
    try {
      await request('/api/resaveGoods', 'POST', { ...form, id: currentItem })
      setGoods(prev => {
        let idx = prev.findIndex(el => +el.id === +currentItem)
        if (idx >= 0) {
          prev[idx] = { ...form, id: currentItem }
        }
        return prev
      })
    } catch (error) {
      console.log(error)
    }
    closeModalEdit()
  }

  async function deleteRow(row) {
    try {
     let res = await request('/api/deleteGoods', 'POST', { row })
      setGoods(res)
    } catch (error) {
      console.log(error)
    }
  }

  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "60px",
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Категория",
      selector: "category",
      sortable: true,
  
    },
    {
      name: "Название",
      selector: "name",
      sortable: true
    },
    {
      name: "Сорт",
      selector: "type",
      sortable: true
    },
    {
      name: "Единицы изм.",
      selector: "unit",
      sortable: true
    },
    {
      name: "Сезон",
      selector: "season",
      sortable: true
    },
    {
      name: "Стандарт",
      selector: "standart",
      sortable: true
    },
    {
      name: "Последний поставщик",
      selector: "lastsupplier",
      sortable: true
    },
    {
      name: "Последняя цена закупки",
      selector: "lastpraice",
      sortable: true
    },
    {
      name: "% наценки",
      selector: "markup",
      sortable: true
    },
    {
      name: "Производитель",
      selector: "manufacturer",
      sortable: true
    },
    {
      name: "Штрихкод",
      selector: "barcode",
      sortable: true
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} menuItems={[{ title: ["Удалить"], onClick: [() => { deleteRow(row) }] }]} />,
      allowOverflow: true,
      button: true,
      compact: true,
    },
  
  ];

  

  if (!goods?.length && !goodsCategory?.length) {
    return (
      <><div id="preloader">
        <div id="loader"></div>
      </div></>
    )
  }
  return (
    <>
      <Layout sidebar={true}>
        <h3>Справочник "Товары"</h3>
        <Line />
        <button type="button" className="btn btn-success" onClick={showModalAdd} >Создать товар</button>
        <DataTable
          onRowClicked={showModalEdit}
          columns={columns}
          data={goods}
          highlightOnHover
          pointerOnHover
        />
      </Layout>


      <Modal centered size="xl" show={showAdd} onHide={closeModalAdd}>
        <Modal.Header>
          <Modal.Title>Создать "Товар"</Modal.Title>
          <Button variant="light" onClick={closeModalAdd}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>

          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Название</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="name"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Сорт</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="type"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >	Штрихкод</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="barcode"
                  />
                </InputGroup>
              </div>
            </div>

          </div>



          <div className="container text-center">
            <div className="row">
              <div className="col">
                <p>Сезонность</p>
                <select className="form-select" aria-label="Default select example" name="season"
                  onChange={changeHandler}
                >
                  <option selected disabled>
                  Не выбрано.
                  </option>
                  {Object.values(seasonality).map((season) => (
                    <option value={season}>{season}</option>
                  ))}
                </select>
              </div>
              <div className="col">
                <p>Единицы изм.</p>
                <select className="form-select" aria-label="Default select example"
                  onChange={changeHandler}
                >
                  <option selected disabled>
                    {"Единицы изм."}
                  </option>
                  {Object.values(units).map((unit) => (
                    <option value={unit}>{unit}</option>
                  ))}
                </select>
              </div>
              <div className="col">
                <p>Категория1</p>
                <Select
                  onChange={changeHandler}
                  className="select"
                  options={goodsCategory}
                  isSearchable="true"
                  placeholder={'категория'}
                  styles={customStylesSelect}
                />
              </div>
              <div className="col">
                <p>Сложность хранения</p>
                <select className="form-select" aria-label="Default select example"
                  onChange={changeHandler}
                >
                  <option selected disabled>
                    {"Сложность хранения"}
                  </option>
                  {Object.values(storagePeriods).map((storagePeriod) => (
                    <option value={storagePeriod}>{storagePeriod}</option>
                  ))}
                </select>
              </div>
            </div>
          </div>

          <hr align="center" width="100%" style={{ color: "gray" }} />


          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Длина, см</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="length"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Ширина, см</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="width"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Высота, см</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="height"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Вес</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="weight"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Стандарт поставки</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="standart"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Производитель</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="manufacturer"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Описание</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="description"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={addClick}>
            Создать
          </Button>
        </Modal.Footer>
      </Modal>



      <Modal centered size="xl" show={showEdit} onHide={closeModalEdit}>
        <Modal.Header>
          <Modal.Title>Изменить "Товар"</Modal.Title>
          <Button variant="light" onClick={closeModalEdit}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>
          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Название</InputGroup.Text>
                  <Form.Control
                    value={form.name}
                    onChange={changeHandler}
                    name="name"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Сорт</InputGroup.Text>
                  <Form.Control
                    value={form.type}
                    onChange={changeHandler}
                    name="type"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth">Штрихкод</InputGroup.Text>
                  <Form.Control
                    value={form.barcode}
                    onChange={changeHandler}
                    name="barcode"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
          <div className="container text-center">
            <div className="row">
              <div className="col">
                <p>Сезонность</p>
                <select className="form-select" aria-label="Default select example" name="season"
                  onChange={changeHandler}
                >
                  <option value="" selected >
                    {form && form.season ? form.season : ""}
                  </option>
                  {Object.values(seasonality).map((season) => (
                    <option value={season}>{season}</option>
                  ))}
                </select>
              </div>
              <div className="col">
                <p>Единицы изм.</p>
                <select className="form-select" aria-label="Default select example" name="unit"
                  onChange={changeHandler}
                >
                  <option value="" selected >
                    {form && form.unit ? form.unit : ""}
                  </option>
                  {Object.values(units).map((unit) => (
                    <option value={unit}>{unit}</option>
                  ))}
                </select>
              </div>
              <div className="col">
                <p>Категория</p>
                <Select
                  name="category"
                  value={selectedCategory}
                  onChange={handleSelectChange}
                  className="select"
                  options={goodsCategory}
                  isSearchable="true"
                  placeholder={'категория'}
                  styles={customStylesSelect}
                />
              </div>
              <div className="col">
                <p>Сложность хранения</p>
                <select className="form-select" aria-label="Default select example" name="storing"
                  onChange={changeHandler}
                >
                  <option selected>
                    {form && form.storing ? form.storing : ""}
                  </option>
                  {Object.values(storagePeriods).map((storagePeriod) => (
                    <option value={storagePeriod}>{storagePeriod}</option>
                  ))}
                </select>
              </div>
            </div>
          </div>

          <hr align="center" width="100%" style={{ color: "gray" }} />

          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Длина, см</InputGroup.Text>
                  <Form.Control
                    value={form.length}
                    onChange={changeHandler}
                    name="length"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Ширина, см</InputGroup.Text>
                  <Form.Control
                    value={form.width}
                    onChange={changeHandler}
                    name="width"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Высота, см</InputGroup.Text>
                  <Form.Control
                    value={form.height}
                    onChange={changeHandler}
                    name="height"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Вес</InputGroup.Text>
                  <Form.Control
                    value={form.weight}
                    onChange={changeHandler}
                    name="weight"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Стандарт поставки</InputGroup.Text>
                  <Form.Control
                    value={form.standart}
                    onChange={changeHandler}
                    name="standart"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Производитель</InputGroup.Text>
                  <Form.Control
                    value={form.manufacturer}
                    onChange={changeHandler}
                    name="manufacturer"
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth_lg" >Описание</InputGroup.Text>
                  <Form.Control
                    value={form.description}
                    onChange={changeHandler}
                    name="description"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={saveClick}>
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

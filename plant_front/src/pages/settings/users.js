import { Layout } from '../../components/Layout/Layout'
import { Line } from '../../components/Line/line'
import React, { useEffect } from 'react'
import SideBar from "../../components/SideBar/sideBar";
import '../../App.css';
import { useState } from 'react';
import DataTable from "react-data-table-component";
import { useHttp } from "../../hooks/http.hook"
import SimpleMenu from '../../components/menu/menu';
// import {
//   Button,
//   Col,
//   Container,
//   Form,
//   FormGroup,
//   Modal,
//   Row
// } from "react-bootstrap";


export const Users = () => {
  const [users, setUsers] = useState(null)
  // const [show, setShow] = useState(false);
  // const closeModal = () => setShow(false);
  // const showModal = (row, event) => {
  //   setShow(true)
  // }
  // const [somedata, setSomedata] = useState(null);
  // const [activeRow, setActiveRow] = useState({});

  // const [showAdd, setShowAdd] = useState(false);
  // const closeModalAdd = () => setShowAdd(false);
  // const showModalAdd = () => {
  //   setShowAdd(true)
  // }

  const { request } = useHttp()

  useEffect(() => {
    (async () => {
      await getAllUsers()
    })()
  }, [])


  async function getAllUsers() {
    try {
      const data = await request('/api/getAllUsers', 'GET')
      setUsers(data.users)
    } catch (error) {
      setUsers([])
    }
  }


  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "30px",
      compact: true,
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Имя",
      selector: "name",
      compact: true,
      sortable: true,
    },
    {
      name: "Email",
      selector: "email",
      compact: true,
      sortable: true,
    },

    {
      name: "Роль",
      selector: "role",
      compact: true,
      sortable: true,
    },

    {
      name: "Состояние",
      selector: "condition",
      compact: true,
      sortable: true,
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} />,
      allowOverflow: true,
      button: true,
      compact: true,
    },

  ];


  if (!users?.length) {
    return (
      <><div id="preloader">
      <div id="loader"></div>
    </div></>
    )
  }
  return (
    <>
      <Layout sidebar={true}>

        <div className='layout'>
          <div className='sidebar'>
            <h3>Главная</h3>
            <Line />
            <SideBar />
          </div>
          <div className='content'>
            <h3>Пользователи</h3>
            <Line />
            {/* <button type="button" class="btn btn-success" onClick={showModalAdd} >Создать запись</button> */}
            <div className='wrp'>
              <DataTable
                responsive={false}
                striped={true}
                pointerOnHover={true}
                // onRowClicked={showModal}
                columns={columns}
                data={users}
                highlightOnHover
              />
            </div>
          </div>
        </div>

      </Layout>

      {/* <Modal size="m" scrollable
        aria-labelledby="contained-modal-title-vcenter"
        centered show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Изменение записи</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody">

          <table className="tab">
            <tr>
              <td>Значение: </td>
              <td>
                <div contentEditable>0</div></td>
            </tr>
          </table>

        </Modal.Body>

        <Modal.Footer>
          <Button variant="success">
            Сохранить
          </Button>
          <Button variant="danger" >
            Удалить
          </Button>
        </Modal.Footer>
      </Modal> */}


      {/* <Modal size="m" scrollable
        aria-labelledby="contained-modal-title-vcenter"
        centered show={showAdd} onHide={closeModalAdd}>
        <Modal.Header>
          <Modal.Title>Добавление записи</Modal.Title>
          <Button variant="light" onClick={closeModalAdd}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody">

          <table className="tab">
            <tr>
              <td>Значение: </td>
              <td>
                <div contentEditable>0</div></td>
            </tr>
          </table>

        </Modal.Body>

        <Modal.Footer>
          <Button variant="success">
            Сохранить
          </Button>
          <Button variant="danger" >
            Отмена
          </Button>
        </Modal.Footer>
      </Modal> */}

    </>
  )
}

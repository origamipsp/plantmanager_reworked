import { Layout } from '../../components/Layout/Layout'
import { Line } from '../../components/Line/line'
import React from 'react'
import SideBar from "../../components/SideBar/sideBar";
import '../../App.css';
import { useState } from 'react';
import {
  Button,
  Modal,
} from "react-bootstrap";


export const EmployeeRegistration = () => {


  const [show, setShow] = useState(false);
  const closeModal = () => setShow(false);
  // const showModal = (row, event) => {
  //   setShow(true)
  // }

  return (
    <>
      <Layout sidebar={true}>
        <div className='layout'>
          <div className='sidebar'>
            <h3>Главная</h3>
            <Line />
            <SideBar />
          </div>
          <div className='content'>
            <h3>Справочник "Единицы измерения"</h3>
            <Line />
          </div>
        </div>
      </Layout>


      <Modal centered bsSize="large" show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Данные клиента</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>

          <table className="editClient">
            <tr>
              <td><strong>Имя</strong></td>
              <td><div contentEditable>Мантрова Любовь Викторовна</div></td>
            </tr>
            <tr>
              <td><strong>Ник</strong></td>
              <td><div contentEditable>Любовь Мантрова</div></td>
            </tr>
            <tr>
              <td><strong>Телефон</strong></td>
              <td><div contentEditable>89806712610</div></td>
            </tr>
            <tr>
              <td><strong>Email</strong></td>
              <td><div contentEditable>@gmail.com</div></td>
            </tr>
          </table>

          <div><strong>Источник</strong><select class="form-select" aria-label="Default select example">
            <option selected value="1">другое</option>
            <option value="2">сми</option>
            <option value="3">Вконтакте</option>
          </select></div>

          <div><strong>Адрес доставки:</strong><div contentEditable>393929 Тамбовская обл Моршанский р-н п Пригородный ул Дачная д 1 кв 2</div></div>
          <div><strong>Комментарий покупателя:</strong><div contentEditable>https://vk.com/gim187250262?sel=246510902</div></div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success">
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>

    </>
  )
}

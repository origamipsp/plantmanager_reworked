import { Layout } from '../../components/Layout/Layout'
import { Line } from '../../components/Line/line'
import React from 'react'
import SideBar from "../../components/SideBar/sideBar";
import '../../App.css';
import { productCategories } from "../../config_data";
import DataTable from "react-data-table-component";



export const ProductCategories = () => {

  // const [show, setShow] = useState(false);
  // const closeModal = () => setShow(false);
  // const showModal = (row, event) => {
  //   setShow(true)
  // }
  // const [somedata, setSomedata] = useState(null);
  // const [activeRow, setActiveRow] = useState({});

  // const [showAdd, setShowAdd] = useState(false);
  // const closeModalAdd = () => setShowAdd(false);
  // const showModalAdd = () => {
  //   setShowAdd(true)
  // }

  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "30px",
      compact: true,
      cell: (row) => <div>{row.id}</div>
    },
    // {
    //   name: "Значение",
    //   selector: "value",
    //   sortable: true,
    // },
  ];



  return (
    <>
      <Layout sidebar={true}>

        <div className='layout'>
          <div className='sidebar'>
            <h3>Главная</h3>
            <Line />
            <SideBar />
          </div>
          <div className='content'>
            <h3>Справочник "Категории товара"</h3>
            <Line />
            {/* <button type="button" class="btn btn-success" onClick={showModalAdd} >Создать запись</button> */}
            <div className='wrp'>
              <DataTable
                responsive={false}
                striped = {true}
                pointerOnHover={true}
                // onRowClicked={showModal}
                columns={columns}
                data={productCategories}
                highlightOnHover
              />
            </div>
          </div>
        </div>


      </Layout>

      {/* <Modal size="m" scrollable
        aria-labelledby="contained-modal-title-vcenter"
        centered show={show} onHide={closeModal}>
        <Modal.Header>
          <Modal.Title>Изменение записи</Modal.Title>
          <Button variant="light" onClick={closeModal}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody">

          <table className="tab">
            <tr>
              <td>Значение: </td>
              <td>
                <div contentEditable>0</div></td>
            </tr>
          </table>

        </Modal.Body>

        <Modal.Footer>
          <Button variant="success">
            Сохранить
          </Button>
          <Button variant="danger" >
            Удалить
          </Button>
        </Modal.Footer>
      </Modal> */}

      {/* <Modal size="m" scrollable
        aria-labelledby="contained-modal-title-vcenter"
        centered show={showAdd} onHide={closeModalAdd}>
        <Modal.Header>
          <Modal.Title>Добавление записи</Modal.Title>
          <Button variant="light" onClick={closeModalAdd}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body className="modalbody">

          <table className="tab">
            <tr>
              <td>Значение: </td>
              <td>
                <div contentEditable>0</div></td>
            </tr>
          </table>

        </Modal.Body>

        <Modal.Footer>
          <Button variant="success">
            Сохранить
          </Button>
          <Button variant="danger" >
            Отмена
          </Button>
        </Modal.Footer>
      </Modal> */}

    </>
  )
}

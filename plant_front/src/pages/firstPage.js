import { Layout } from '../components/Layout/Layout'
import React from 'react'
import { ChartClientsByMonth } from '../components/ChartClientsByMonth/ChartClientsByMonth'
import { ChartByCategory } from '../components/ChartByCategory/ChartByCategory'
import { ChartByStatus } from '../components/ChartByStatus/ChartByStatus'
import { ChartBySeason} from '../components/ChartBySeason/ChartBySeason'


const line ={
  width: "50%",
  height: "2px",
  backgroundColor: "#b8860b"
}

export const FirstPage = () => {
  return (
    <Layout sidebar={true}>
      <h3>Статистика</h3>
      <div style={line}></div>
    <ChartClientsByMonth />
    <ChartBySeason/>
    <ChartByStatus />
    <ChartByCategory />
  </Layout>
  )
}

import { Layout } from '../components/Layout/Layout'
import { Line } from '../components/Line/line'
import React, { useEffect } from 'react'
import { useHttp } from "../hooks/http.hook"
import { useState } from 'react';
import {
  Button,
  Modal,
  InputGroup,
  Form,
} from "react-bootstrap";
import SimpleMenu from '../components/menu/menu';
import DataTable from "react-data-table-component";


export const SupplierDirectory = () => {

  const { request } = useHttp()

  const [form, setForm] = useState({
    company: "",
    phone: "",
    email: "",
    comment: "",
    address: "",
  })

  const [suppliers, setSuppliers] = useState(null)
  const [currentSupplier, setCurrentSupplier] = useState(null)

  const [showAdd, setShowAdd] = useState(false);
  const closeModalAdd = () => setShowAdd(false);
  const showModalAdd = (row, event) => {
    setShowAdd(true)
  }

  const [showEdit, setShowEdit] = useState(false);
  const closeModalEdit = () => setShowEdit(false);

  const showModalEdit = (row, event) => {
    setForm(
      {
        company: row.company,
        phone: row.phone,
        email: row.email,
        comment: row.comment,
        address: row.address,
      }
    )
    setShowEdit(true)
    setCurrentSupplier(row.id)
  }

  useEffect(() => {
    (async () => {
      await getAllSuppliers()
    })()
  }, [])


  const changeHandler = e => {
    setForm(prev => ({ ...prev, [e.target.name]: e.target.value }))
  }


  async function getAllSuppliers() {
    try {
      const data = await request('/api/getAllSuppliers', 'GET')
      setSuppliers(data.suppliers) ///
    } catch (error) {
      setSuppliers([])
    }
  }

  //const [activeRow, setActiveRow] = useState({});

  async function addClick() {
    try {
      setSuppliers(await request('/api/addSupplier', 'POST', { ...form }))
    } catch (error) {
      console.log(error)
    }
    closeModalAdd()
  }

  async function saveClick() {
    console.log("form-", { ...form, id: currentSupplier })
    try {
      await request('/api/resaveSupplier', 'POST', { ...form, id: currentSupplier })
      setSuppliers(prev => {
        let idx = prev.findIndex(el => +el.id === +currentSupplier)
        if (idx >= 0) {
          prev[idx] = { ...form, id: currentSupplier }
        }
        return prev
      })
    } catch (error) {
      console.log(error)
    }
    closeModalEdit()
  }



  async function deleteRow(row) {
    try {
     let res = await request('/api/deleteSupplier', 'POST', { row })
      setSuppliers(res)
    } catch (error) {
      console.log(error)
    }
  }

  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "60px",
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Компания",
      selector: "company",
      sortable: true,

    },
    {
      name: "Телефон",
      selector: "phone",
      sortable: true
    },
    {
      name: "Email",
      selector: "email",
      sortable: true
    },
    {
      name: "Комментарий",
      selector: "comment",
      sortable: true
    },
    {
      name: "Адрес",
      selector: "address",
      sortable: true
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} menuItems={[{ title: ["Удалить"], onClick: [() => { deleteRow(row) }] }]} />,
      allowOverflow: true,
      button: true,
      compact: true,
    },
  ];



  if (!suppliers?.length) {
    return (
      <><div id="preloader">
        <div id="loader"></div>
      </div></>
    )
  }
  return (
    <>
      <Layout sidebar={true}>
        <h3>Справочник "Поставщики"</h3>
        <Line />
        <button type="button" class="btn btn-success" onClick={showModalAdd} >Создать поставщика</button>

        <DataTable
          onRowClicked={showModalEdit}
          columns={columns}
          data={suppliers}
          highlightOnHover
        />

      </Layout>

      <Modal centered size="xl" show={showAdd} onHide={closeModalAdd}>
        <Modal.Header>
          <Modal.Title>Добавить "Поставщика"</Modal.Title>
          <Button variant="light" onClick={closeModalAdd}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>
          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Компания</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="company"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Телефон</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="phone"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Email</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="email"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Адрес</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="address"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Комментарий</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="comment"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={addClick}>
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>





      <Modal centered size="xl" show={showEdit} onHide={closeModalEdit}>
        <Modal.Header>
          <Modal.Title>Изменить "Поставщика"</Modal.Title>
          <Button variant="light" onClick={closeModalEdit}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>
          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Компания</InputGroup.Text>
                  <Form.Control
                    value={form.company}
                    onChange={changeHandler}
                    name="company"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Телефон</InputGroup.Text>
                  <Form.Control
                    value={form.phone}
                    onChange={changeHandler}
                    name="phone"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Email</InputGroup.Text>
                  <Form.Control
                    value={form.email}
                    onChange={changeHandler}
                    name="email"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Адрес</InputGroup.Text>
                  <Form.Control
                    value={form.address}
                    onChange={changeHandler}
                    name="address"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Комментарий</InputGroup.Text>
                  <Form.Control
                    value={form.comment}
                    onChange={changeHandler}
                    name="comment"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={saveClick}>
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>

    </>
  )
}

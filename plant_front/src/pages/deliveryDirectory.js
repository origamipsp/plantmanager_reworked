import { Layout } from '../components/Layout/Layout'
import { Line } from '../components/Line/line'
import React, { useEffect } from 'react'
import { useHttp } from "../hooks/http.hook"
import { useState } from 'react';
import {
  Button,
  Modal,
  InputGroup,
  Form,
} from "react-bootstrap";
import SimpleMenu from '../components/menu/menu';
import DataTable from "react-data-table-component";


export const DeliveryDirectory = () => {

  const { request } = useHttp()

  const [form, setForm] = useState({
    company: "",
    tariff: "",
    price: "",
  })

  const [deliveries, setDeliveries] = useState(null)
  const [currentCompany, setCurrentCompany] = useState(null)

  const [showAdd, setShowAdd] = useState(false);
  const closeModalAdd = () => setShowAdd(false);
  const showModalAdd = (row, event) => {
    setShowAdd(true)
  }

  const [showEdit, setShowEdit] = useState(false);
  const closeModalEdit = () => {
    setShowEdit(false)
    setForm(
      {
        company: null,
        tariff: null,
        price: null,
      }
    )
  }

  const showModalEdit = (row, event) => {
    setForm(
      {
        company: row.company,
        tariff: row.tariff,
        price: row.price,
      }
    )
    setShowEdit(true)
    setCurrentCompany(row.id)
  }

  useEffect(() => {
    (async () => {
      await getAllDelivery()
    })()
  }, [])


  const changeHandler = e => {
    setForm(prev => ({ ...prev, [e.target.name]: e.target.value }))
  }


  async function getAllDelivery() {
    try {
      const data = await request('/api/getAllDelivery', 'GET')
      setDeliveries(data.deliveries)
    } catch (error) {
      setDeliveries([])
    }
  }

  async function addClick() {
    try {
      setDeliveries(await request('/api/addDelivery', 'POST', { ...form }))
    } catch (error) {
      console.log(error)
    }
    closeModalAdd()
  }

  async function saveClick() {
    console.log("form-", { ...form, id: currentCompany })
    try {
      await request('/api/resaveDelivery', 'POST', { ...form, id: currentCompany })
      setDeliveries(prev => {
        let idx = prev.findIndex(el => +el.id === +currentCompany)
        if (idx >= 0) {
          prev[idx] = { ...form, id: currentCompany }
        }
        return prev
      })
    } catch (error) {
      console.log(error)
    }
    closeModalEdit()
  }

  async function deleteRow(row) {
    try {
      let res = await request('/api/deleteDelivery', 'POST', { row })
      setDeliveries(res)
    } catch (error) {
      console.log(error)
    }
  }

  const columns = [
    {
      name: "#",
      selector: "id",
      sortable: true,
      width: "60px",
      cell: (row) => <div>{row.id}</div>
    },
    {
      name: "Компания",
      selector: "company",
      sortable: true,

    },
    {
      name: "Тариф",
      selector: "tariff",
      sortable: true
    },
    {
      name: "Стоимость, руб/кг",
      selector: "price",
      sortable: true
    },
    {
      name: "Действие",
      cell: row => <SimpleMenu size="small" row={row} menuItems={[{ title: ["Удалить"], onClick: [() => { deleteRow(row) }] }]} />,
      allowOverflow: true,
      button: true,
      compact: true,
    },
  ];

  if (!deliveries?.length) {
    return (
      <><div id="preloader">
        <div id="loader"></div>
      </div></>
    )
  }
  return (
    <>
      <Layout sidebar={true}>
        <h3>Справочник "Доставка"</h3>
        <Line />
        <button type="button" className="btn btn-success" onClick={showModalAdd} >Создать "Доставку"</button>
        <DataTable
          onRowClicked={showModalEdit}
          columns={columns}
          data={deliveries}
          highlightOnHover
          pointerOnHover
        />
      </Layout>

      <Modal centered bsSize="large" show={showAdd} onHide={closeModalAdd}>
        <Modal.Header>
          <Modal.Title>Создать "Доставку"</Modal.Title>
          <Button variant="light" onClick={closeModalAdd}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>
          <InputGroup size="sm" className="mb-3">
            <InputGroup.Text className="inputNameWidth" >Компания</InputGroup.Text>
            <Form.Control
              onChange={changeHandler}
              name="company"
            />
          </InputGroup>
          <InputGroup size="sm" className="mb-3">
            <InputGroup.Text className="inputNameWidth">Тариф</InputGroup.Text>
            <Form.Control
              onChange={changeHandler}
              name="tariff"
            />
          </InputGroup>
          <InputGroup size="sm" className="mb-3">
            <InputGroup.Text className="inputNameWidth" >Цена, руб/кг</InputGroup.Text>
            <Form.Control
              type="number"
              onChange={changeHandler}
              name="price"
            />
          </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={addClick}>
            Создать
          </Button>
        </Modal.Footer>
      </Modal>



      <Modal centered bsSize="large" show={showEdit} onHide={closeModalEdit}>
        <Modal.Header>
          <Modal.Title>Изменить "Доставку"</Modal.Title>
          <Button variant="light" onClick={closeModalEdit}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>
          <InputGroup size="sm" className="mb-3">
            <InputGroup.Text className="inputNameWidth" >Компания</InputGroup.Text>
            <Form.Control
              value={form.company}
              onChange={changeHandler}
              name="company"
            />
          </InputGroup>
          <InputGroup size="sm" className="mb-3">
            <InputGroup.Text className="inputNameWidth" >Тариф</InputGroup.Text>
            <Form.Control
              value={form.tariff}
              onChange={changeHandler}
              name="tariff"
            />
          </InputGroup>
          <InputGroup size="sm" className="mb-3">
            <InputGroup.Text className="inputNameWidth" >Цена, руб/кг</InputGroup.Text>
            <Form.Control
              type="number"
              value={form.price}
              onChange={changeHandler}
              name="price"
            />
          </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={saveClick}>
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

import { Layout } from '../components/Layout/Layout'
import { StorageTabs } from '../components/StorageTabs/storageTabs'
import { Line } from '../components/Line/line'
import React from 'react'


export const Storage = () => {
    return (
        <Layout sidebar={true}>
            <h3>Склад</h3>
            <Line />
            <StorageTabs />
        </Layout>
    )
}

import { Layout } from '../components/Layout/Layout'
import { ClientOrderTabs } from '../components/ClientOrderTabs/clientOrderTabs'
import { Line } from '../components/Line/line'
import React from 'react'


export const ClientOrder = () => {
    return (
        <Layout sidebar={true}>
            <h3>Заказы клиентов</h3>
            <Line />
            <ClientOrderTabs />
        </Layout>
    )
}

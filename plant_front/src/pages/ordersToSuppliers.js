import { Layout } from '../components/Layout/Layout'
import { OrderTabs } from '../components/OrderTabs/orderTabs'
import { Line } from '../components/Line/line'
import React from 'react'
import { useState } from "react";
import "../components/Tables/table.css";


export const OrdersToSuppliers = () => {
    const [hideUpload, setHideUpload] = useState(true);
    return (
        <>
            <Layout sidebar={true}>
                <h3>Заказы поставщикам</h3>
                <Line />
                <button type="button" className="btn btn-outline-success" onClick={() => setHideUpload(prev => !prev)}>Импорт заказа</button>
                <div className={`${hideUpload ? "hide-lightbox" : "lightbox"}`}   >
                    <Line />
                    <form>
                        <div class="form-group">
                            <input type="file" class="form-control-file" id="exampleFormControlFile1"></input>
                        </div>
                    </form>
                    <button type="button" class="btn btn-secondary">Загрузить файл</button>
                </div>
                <OrderTabs />
            </Layout>
        </>
    )
}

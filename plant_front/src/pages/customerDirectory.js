import { Layout } from '../components/Layout/Layout'
import { Line } from '../components/Line/line'
import React, { useEffect } from 'react'
import { useHttp } from "../hooks/http.hook"
import DataTable from "react-data-table-component";
import { useState } from 'react';
import {
  Button,
  Modal,
  InputGroup,
  Form,
} from "react-bootstrap";
import { sources } from '../data/select_config_data'
import SimpleMenu from '../components/menu/menu';



export const CustomerDirectory = () => {

  const { request } = useHttp()

  const [form, setForm] = useState({
    name: "",
    nickName: "",
    phone: "",
    email: "",
    source: "",
    address: "",
    comment: "",
  })


  const [customer, setCustomer] = useState(null)
  const [currentCustomer, setCurrentCustomer] = useState(null)

  const [showAdd, setShowAdd] = useState(false);
  const closeModalAdd = () => setShowAdd(false);
  const showModalAdd = (row, event) => {
    setShowAdd(true)
  }

  const [showEdit, setShowEdit] = useState(false);
  const closeModalEdit = () => setShowEdit(false);

  const showModalEdit = (row, event) => {
    setForm(
      {
        name: row.name,
        nickName: row.nickName,
        phone: row.phone,
        email: row.email,
        source: row.source,
        address: row.address,
        comment: row.comment,
      }
    )
    setShowEdit(true)
    setCurrentCustomer(row.id)
  }


  useEffect(() => {
    (async () => {
      console.log(" useEffect")
      await getAllCustomers()
    })()
  }, [])

  const changeHandler = e => {
    setForm(prev => ({ ...prev, [e.target.name]: e.target.value }))
  }


  async function getAllCustomers() {
    try {
      const data = await request('/api/getAllCustomers', 'GET')
      console.log("data.customers-", data.customers)
      setCustomer(data.customers) ///
    } catch (error) {
      setCustomer([])
    }
  }

  async function addClick() {
    try {
      setCustomer(await request('/api/addCustomer', 'POST', { ...form }))
    } catch (error) {
      console.log(error)
    }
    closeModalAdd()
  }


  async function saveClick() {
    console.log("form-", { ...form, id: currentCustomer })
    try {
      await request('/api/resaveCustomer', 'POST', { ...form, id: currentCustomer })
      setCustomer(prev => {
        let idx = prev.findIndex(el => +el.id === +currentCustomer)
        if (idx >= 0) {
          prev[idx] = { ...form, id: currentCustomer }
        }
        return prev
      })
    } catch (error) {
      console.log(error)
    }
    closeModalEdit()
  }

  async function deleteRow(row) {
    try {
     let res = await request('/api/deleteCustomer', 'POST', { row })
      setCustomer(res)
    } catch (error) {
      console.log(error)
    }
  }

  
const columns = [
  {
    name: "#",
    selector: "id",
    sortable: true,
    width: "60px",
    cell: (row) => <div>{row.id}</div>
  },
  {
    name: "Имя",
    selector: "name",
    sortable: true,

  },
  {
    name: "Ник",
    selector: "nickName",
    sortable: true
  },
  {
    name: "Телефон",
    selector: "phone",
    sortable: true
  },
  {
    name: "Email",
    selector: "email",
    sortable: true
  },
  {
    name: "Источник",
    selector: "source",
    sortable: true
  },
  {
    name: "Действие",
    cell: row => <SimpleMenu size="small" row={row} menuItems={[{ title: ["Удалить"], onClick: [() => { deleteRow(row) }] }]} />,
    allowOverflow: true,
    button: true,
    compact: true,
  },
];


  if (!customer?.length) {
    return (
      <><div id="preloader">
        <div id="loader"></div>
      </div></>
    )
  }

  return (
    <>
      <Layout sidebar={true}>
        <h3>Справочник "Клиенты"</h3>
        <Line />
        <button type="button" className="btn btn-success" onClick={showModalAdd} >Создать "Клиента"</button>

        <DataTable
          onRowClicked={showModalEdit}
          columns={columns}
          data={customer}
          highlightOnHover
        />

      </Layout>


      <Modal centered size="xl" show={showAdd} onHide={closeModalAdd}>
        <Modal.Header>
          <Modal.Title>Создать "Клиента"</Modal.Title>
          <Button variant="light" onClick={closeModalAdd}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>


          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Имя</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="name"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Ник</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="nickName"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Телефон</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="phone"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Email</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="email"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <select className="form-select" aria-label="Default select example" name="source"
                  onChange={changeHandler}
                >
                  <option selected disabled>
                    {"Источник"}
                  </option>
                  {Object.values(sources).map((source) => (
                    <option value={source}>{source}</option>
                  ))}
                </select>
              </div>
            </div>
          </div>

          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Адрес доставки:</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="address"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Комментарий покупателя:</InputGroup.Text>
                  <Form.Control
                    onChange={changeHandler}
                    name="comment"
                  />
                </InputGroup>
              </div>
            </div>
          </div>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={addClick}>
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>




      <Modal centered size="xl" show={showEdit} onHide={closeModalEdit}>
        <Modal.Header>
          <Modal.Title>Изменить "Клиента"</Modal.Title>
          <Button variant="light" onClick={closeModalEdit}>
            X
          </Button>
        </Modal.Header>
        <Modal.Body>


          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Имя</InputGroup.Text>
                  <Form.Control
                    value={form.name}
                    onChange={changeHandler}
                    name="name"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Ник</InputGroup.Text>
                  <Form.Control
                    value={form.nickName}
                    onChange={changeHandler}
                    name="nickName"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Телефон</InputGroup.Text>
                  <Form.Control
                    value={form.phone}
                    onChange={changeHandler}
                    name="phone"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Email</InputGroup.Text>
                  <Form.Control
                    value={form.email}
                    onChange={changeHandler}
                    name="email"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <select className="form-select" aria-label="Default select example" name="source"
                  onChange={changeHandler}
                >
                  <option value="" selected >
                    {form && form.source ? form.source : ""}
                  </option>
                  {Object.values(sources).map((source) => (
                    <option value={source}>{source}</option>
                  ))}
                </select>
              </div>
            </div>
          </div>

          <div className="container text-center">
            <div className="row">
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Адрес доставки:</InputGroup.Text>
                  <Form.Control
                    value={form.address}
                    onChange={changeHandler}
                    name="address"
                  />
                </InputGroup>
              </div>
              <div className="col">
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text className="inputNameWidth" >Комментарий покупателя:</InputGroup.Text>
                  <Form.Control
                    value={form.comment}
                    onChange={changeHandler}
                    name="comment"
                  />
                </InputGroup>
              </div>
            </div>
          </div>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={saveClick}>
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

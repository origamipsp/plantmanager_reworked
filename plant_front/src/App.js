import './App.css';
import { FirstPage } from './pages/firstPage'
import { Storage } from './pages/storage'
import { ClientOrder } from './pages/clientOrder'
import { OrdersToSuppliers } from './pages/ordersToSuppliers'
import { CustomerDirectory } from './pages/customerDirectory'
import { SupplierDirectory } from './pages/supplierDirectory'
import { GoodsDirectory } from './pages/goodsDirectory'
import { DeliveryDirectory } from './pages/deliveryDirectory'
//settings
import { CustomerOrderStatus } from './pages/settings/customerOrderStatus'
import { CustomerSource } from './pages/settings/customerSource'
import { DeliveryTime } from './pages/settings/deliveryTime'
import { EmployeeRegistration } from './pages/settings/employeeRegistration'
import { ItemStatus } from './pages/settings/itemStatus'
import { PaymentPurpose } from './pages/settings/paymentPurpose'
import { PaymentType } from './pages/settings/paymentType'
import { ProductCategories } from './pages/settings/productCategories'
import { Seasonality } from './pages/settings/seasonality'
import { StoragePeriods } from './pages/settings/storagePeriods'
import { Storages } from './pages/settings/storages'
import { SupplierOrderStatus } from './pages/settings/supplierOrderStatus'
import { Units } from './pages/settings/units'
import { Users } from './pages/settings/users'
import { WriteOffReasons } from './pages/settings/writeOffReasons'

import { Routes, Route } from 'react-router-dom'

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<FirstPage />} />
        <Route path="/orderstosuppliers" element={<OrdersToSuppliers />} />
        <Route path="/storage" element={<Storage />} />
        <Route path="/clientorder" element={<ClientOrder/>} />
        <Route path="/customerdirectory" element={<CustomerDirectory />} />
        <Route path="/supplierdirectory" element={<SupplierDirectory />} />
        <Route path="/goodsdirectory" element={<GoodsDirectory />} />
        <Route path="/deliverydirectory" element={<DeliveryDirectory />} />
        {/* settings */}
        <Route path="/settings/units" element={<Units />} />
        <Route path="/settings/deliverytime" element={<DeliveryTime />} />
        <Route path="/settings/seasonality" element={<Seasonality />} />
        <Route path="/settings/customersource" element={<CustomerSource />} />
        <Route path="/settings/storageperiods" element={<StoragePeriods />} />
        <Route path="/settings/paymenttype" element={<PaymentType />} />
        <Route path="/settings/paymentpurpose" element={<PaymentPurpose />} />
        <Route path="/settings/customerorderstatus" element={<CustomerOrderStatus />} />
        <Route path="/settings/supplierorderstatus" element={<SupplierOrderStatus />} />
        <Route path="/settings/itemstatus" element={<ItemStatus />} />
        <Route path="/settings/storages" element={<Storages />} />
        <Route path="/settings/writeoffreasons" element={<WriteOffReasons />} />
        <Route path="/settings/productcategories" element={<ProductCategories />} />
        <Route path="/settings/employeeregistration" element={<EmployeeRegistration />} />
        <Route path="/settings/users" element={<Users />} />
      </Routes>
    </>
  );
}
export default App;

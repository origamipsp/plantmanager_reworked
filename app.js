const express = require('express')
const config = require('config')
const path = require('path')
const fileUpload = require('express-fileupload')
const db = require('./models/index.js')
// const {runMigrations} = require('./migration')
// const db = require('./db')
// const openConnection = require('./db')
// const closeConnection = require('./db')

// change require to import
// 1. put this -- "type": "module", -- to package.json
// then use import instead require
// import express from 'express'
// import config from 'config'
// import path from 'path'
// import fileUpload from 'express-fileupload'

const app = express()

app.use(express.json({ extended: true }))
app.use(fileUpload({ createParentPath: true }))
app.use('/api', require('./routes/plant.routes'))

if (process.env.NODE_ENV === 'production') {
  app.use('/', express.static(path.join(__dirname, 'plant_front', 'build')))

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'plant_front', 'build', 'index.html'))
  })
}

(async () => {
  await db.sequelize.sync()
})()


const urlencodedParser = express.urlencoded({ extended: false });



// working test
app.get('/', async (req, res) => {
  res.status(200).json({ message: `feels good` })
})

const PORT = process.env.PORT || config.get('port') || 5001

async function start() {
  try {
    app.listen(PORT, () => {
      console.log(`Server has been started on ${PORT}`)
    })

  } catch (e) {
    console.log(`Server error: ${e.message}`)
    process.exit(1)
  }
}

start()